<?php
class Calendar_m extends MY_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database("locman");
    }

     public function getTaskInfos($ids, $start, $due) {
        // $sql =  "select ob2.id as prjid, ob2.name as prjname, ob2.startDate as prjStart, ".
        //         "   ob2.dueDate as prjDue, ob2.status_id as prjStatus, ".
        //         "   ob2.bid_time as prjBidtime, ob1.id as id, ob1.name as name, ".
        //         "   ob1.startDate as startDate, ".
        //         "   ob2.dueDate as dueDate, ob1.bid_time as bid_time, ".
        //         "   ob1.status_id as status_id from objects as ob1 ".
        //         "join (select id, name, startDate, dueDate, bid_time, status_id from objects ".
        //         "   where projects_id is null and startDate is not null and dueDate is not null and del_YN = 'N' ".
        //         "   order by startDate, id) as ob2 on ob1.projects_id = ob2.id ".
        //         "where ob1.type_id = 3 and ob1.startDate is not null and ob1.dueDate is not null and ob1.del_YN = 'N' ".
        //         "   and ob2.id in (".$id.") ".
        //         "order by ob2.startDate, ob1.startDate";
        // echo $sql."<br>";
        $sql =  "select obs2.id as prjid, obs2.name as prjname, obs2.startDate as prjStart, ".
                "   obs2.dueDate as prjDue, obs2.status_id as prjStatus, ".
                "   obs2.bid_time as prjBidtime, obs1.id, obs1.name, obs1.startDate, obs1.dueDate, ".
                "   obs1.bid_time, obs1.status_id from objects obs1 ".
                "join objects obs2 on obs1.projects_id = obs2.id ".
                "where obs1.type_id >= 3 and obs1.del_YN = 'N' and obs1.startDate is not null ".
                "   and obs1.dueDate is not null ".
                "   and obs1.projects_id in (".$ids.") ";
        if (!empty($start))
            $sql = $sql."   and not (obs1.startDate < '".$start."' and obs1.dueDate < '".$start."') ";
        if (!empty($due))
            $sql = $sql."   and not (obs1.startDate > '".$due."' and obs1.dueDate > '".$due."') ";
        $sql = $sql."order by obs2.startDate, obs1.startDate";

        $res = $this->db->query($sql);
        // $result = $res->result_array();
        
        return $res->result_array();
    }

    public function getProjectsList($start, $due) {
        // $sql = "select id, name, startDate, dueDate, ifnull(ob2.cnt, 0) as taskCnt from objects as ob1 ".
        //        "left outer join (select count(id) as cnt, projects_id from objects ".
        //        "    where type_id = 3 and del_YN = 'N' and startDate is not null and dueDate is not null ".
        //        "    group by projects_id) as ob2 on ob2.projects_id = ob1.id ".
        //        "where ob1.type_id = 0 and del_YN = 'N' and startDate is not null and dueDate is not null ";
        $sql = "select id, name, startDate, dueDate, ifnull(ob2.cnt, 0) as taskCnt from objects as ob1 ".
               "left outer join (select count(id) as cnt, projects_id from objects ".
               "    where type_id = 3 and del_YN = 'N' and startDate is not null and dueDate is not null ".
               "    group by projects_id) as ob2 on ob2.projects_id = ob1.id ".
               "where ob1.id in (13056) and ob1.type_id = 0 and del_YN = 'N' and startDate is not null ".
               "    and dueDate is not null ";
        if (!empty($start))
            $sql = $sql."        and not (ob1.startDate < '".$start."' and ob1.dueDate < '".$start."') ";
        if (!empty($due))
            $sql = $sql."        and not (ob1.startDate > '".$due."' and ob1.dueDate > '".$due."') ";
        $sql = $sql."order by ob1.startDate";
        $res = $this->db->query($sql);
        // $res = $this->db->query("select id, name, startDate, dueDate from objects 
        //         where projects_id is null and startDate is not null and dueDate is not null and del_YN = 'N'
        //         order by startDate, id");
        
        return $res->result_array();
    }
}
?>