<?php
class Calendar_c extends My_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->model('timeScheduler/calendar_m', 'calendar_m');

        if (!$this->is_logged_in())
        {
            redirect('index', 'refresh');
        }
    }

    function index() {
        if ($this->is_logged_in()) {
            redirect('index', 'refresh');
        } else {
            redirect('index', 'refresh');
        }
    }

    public function getItemsNSections() {
        $this->yield = FALSE;
        $post = $this->input->post();
        $start = $post['start'];
        $due = $post['due'];

        $data['sections'] = $this->calendar_m->getProjectsList($start, $due);
        if (empty($data['sections'])) {
            echo json_encode(array('status' => 'error', 'message' => 'section is not exist', 'start' => $start, 'due' => $due));
            return;
        }

        $result_id = array();
        foreach ( $data['sections'] as $val) {
            array_push($result_id, $val['id']);
        }
        // for ($i=0; $i<3; $i++) {
        //     array_push($result_id, $data['sections'][$i]['id']);
        // }
        if (count($result_id) > 1)
            $ids = implode(',', $result_id);
        else
            $ids = $result_id[0];

        $data['items'] = $this->calendar_m->getTaskInfos($ids, $start, $due);

        echo json_encode(array('status' => 'success', 'data' => $data));
    }

    public function getProjectsNTasks() {
        $this->yield = FALSE;
        $post = $this->input->post();
        $start = $post['start'];
        $due = $post['due'];

        // echo('test1');
        // print_r('test2');
        // print_r($post);
        // print_r($start);
        // print_r($due);

        $data['projects'] = $this->calendar_m->getProjectsList($start, $due);
        if (empty($data['projects'])) {
            echo json_encode(array('status'=>'error', 'message'=>'section is not exist'));
            return;
        }

        $result_id = array();
        foreach ($data['projects'] as $val) {
            array_push($result_id, $val['id']);
        }
        $ids = implode(',', $result_id);

        $data['tasks'] = $this->calendar_m->getTaskInfos($ids, $start, $due);

        echo json_encode(array('status' => 'success', 'data' => $data));
    }
}
?>