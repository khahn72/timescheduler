class KScheduleModel {
    constructor(start, due) {
        this.projects = [];
        this.tasks = [];
        this.items = [];

        this.rowArray = [];

        this.itemHeight = 18;
        
        this.start = start;
        this.due = due;

        this.days = Math.abs(moment(start).diff(moment(due), 'days')) + 1;
        console.log(this.days, start, due);
    }

    // get projects () {
    //     return this.projects;
    // }
    // set projects (datas) {
    //     this.projects = datas;
    // }

    setProjects (datas) {
        // for (var i=0; i < datas.length; i++) {
        //     var projectInfo = {};
        //     projectInfo["id"] = datas[i].id;
        //     projectInfo["name"] = datas[i].name;
        //     projectInfo["start"] = datas[i].startDate;
        //     projectInfo["due"] = datas[i].dueDate;
        //     projectInfo["taskcnt"] = datas[i].taskCnt;

        //     this.projects.push(projectInfo);
        // }
        // console.log(this.projects);
        this.projects = datas;
        console.log(this.projects);
    }

    setTasks(datas) {
        this.tasks = datas;
        this.setItemDatas(datas);
    }

    setItemDatas (datas) {
        if (this.projects.length <= 0) {
            console.log("cannot found projects");
            return;
        }

        for (var i=0; i < this.projects.length; i++) {
            var item1 = {};
            var item2 = {};
            var prjItem = [];

            // 자동 sort를 위해 item에 project를 두지 않는다.
            // item2["id"] = this.projects[i].id;
            // item2["name"] = this.projects[i].name;
            // item2["start"] = this.projects[i].startDate;
            // item2["due"] = this.projects[i].dueDate;
            // item2["bidtime"] = this.projects[i].bid_time;
            // item2["stat"] = this.projects[i].status_id;

            // prjItem.push(item2);

            for (var j=0; j < datas.length; j++) {
                item2 = {};
                if (this.projects[i].id == datas[j].prjid) {
                    item2.id = datas[j].id;
                    item2.name = datas[j].name;
                    item2.start = datas[j].startDate;
                    item2.due = datas[j].dueDate;
                    item2.bidtime = datas[j].bid_time;
                    item2.stat = datas[j].status_id;

                    prjItem.push(item2);
                }
            }

            item1[String(this.projects[i].id)] = prjItem;
            this.items.push(item1);
        }
        // console.log("this.items:", this.items, this.items.length);
        // for (var key in this.items) {
        //     console.log(key, this.items[key]);
        // }
    }

    getItemOfPrjId (prjid) {
        var itemId = -1;
        for (var i=0; i < this.items.length; i++) {
            if (itemId >= 0)
                break;
            for (var key in this.items[i]) {
                if (key == prjid) {
                    itemId = i;
                    console.log("itemIndex:", itemId)
                    break;
                }
            }
        }

        return [itemId, this.items[itemId]];
    }

    // BITNO_64(x) {return ((x) >> 32) ? 32 + this.BITNO_32((x) >> 32) : this.BITNO_32((x))}
    // BITNO_32(x) {return ((x) >> 16) ? 16 + this.BITNO_16((x) >> 16) : this.BITNO_16((x))}
    BITNO_16(x) {return ((x) >> 8) ? 8 + this.BITNO_8((x) >> 8) : this.BITNO_8((x))}
    BITNO_8(x) {return ((x) >> 4) ? 4 + this.BITNO_4((x) >> 4) : this.BITNO_4((x))}
    BITNO_4(x) {return ((x) >> 2) ? 2 + this.BITNO_2((x) >> 2) : this.BITNO_2((x))}
    BITNO_2(x) {return ((x) & 2) ? 1 : 0}

    BITNO(x) {
        var res = 0;
        for (var i=0; i<16; i++) {
            if (x & (1 << i)) res++;
        }
        return res;
    }

    makeHex (order, count) {
        if (order == 0)
            return this.makePreOrderedHex(count);
        else
            return this.makePostOrderedHex(count);
    }

    // 앞자리 채우기
    makePreOrderedHex (count) {
        var fillnum = 0xf;
        var dummyMask = new Array();
        // console.log("count", count);
        for (var i=0; i < Math.floor(count/4); i++) {
            dummyMask.push(fillnum);
        }
        // 16   1111 1111 1111 1111 (0xffff)
        // 15   1111 1111 1111 1110 (0xfffe)
        // 14   1111 1111 1111 1100 (0xfffc)
        // 13   1111 1111 1111 1000 (0xfff8)
        // 12   1111 1111 1111 0000 (0xfff0)
        // 11   1111 1111 1110 0000 (0xffe0)
        // 10   1111 1111 1100 0000 (0xffc0)
        // 09   1111 1111 1000 0000 (0xff80)
        // 08   1111 1111 0000 0000 (0xff00)
        // 07   1111 1110 0000 0000 (0xfe00)
        // 06   1111 1100 0000 0000 (0xfc00)
        // 05   1111 1000 0000 0000 (0xf800)
        // 04   1111 0000 0000 0000 (0xf000)
        // 03   1110 0000 0000 0000 (0xe000)
        // 02   1100 0000 0000 0000 (0xc000)
        // 01   1000 0000 0000 0000 (0x8000)
        var curMask = 0x0;
        if (count%4 == 1)
            curMask = 0x8;
        else if (count%4 == 2)
            curMask = 0xc;
        else if (count%4 == 3)
            curMask = 0xe;
        // else 
        //     mask = 0xf;
        dummyMask.push(curMask);
        
        var mask1Cnt = dummyMask.length;
        for (var i=0; i < 4-mask1Cnt; i++) {
            dummyMask.push(0x0);
        }
        // console.log("dummyMask3", dummyMask);
        // console.log((dummyMask[0]<<12).toString(2), (dummyMask[0]<<12).toString(16));
        // console.log((dummyMask[0]<<12 | dummyMask[1]<<8 | dummyMask[2]<<4 | dummyMask[3]).toString(16));
        var mask = dummyMask[0]<<12 | dummyMask[1]<<8 | dummyMask[2]<<4 | dummyMask[3];
        // console.log("mask", mask.toString(16));

        return mask;
    }

    // item 시작일/종료일 확인 후 시작/종료 위치 찾기
    // 시작/종료 위치에 2진수 설정
    setItemDayPosition (strstart, strdue) {
        var start = moment(strstart);
        var due = moment(strdue);
        var momentStart = moment(Math.max(start, moment(this.start)));
        var momentEnd = moment(Math.min(due, moment(this.due)));

        var startPos = momentStart.diff(moment(this.start), 'days');
        var endPos = Math.abs(momentStart.diff(momentEnd, 'days')) + 1;

        var calcTop = 0;
        var calcLeft = (startPos / this.days) * 100;
        var calcWidth = (endPos / this.days) * 100;

        // console.log(startPos, endPos, calcLeft, calcWidth);

        // var days = 92;
        // var start = 92-19;
        // var endPos = 9;

        // 총 자릿수 구하기
        var maxCnt = this.days - startPos;
        // bit 수 구하기
        var bitCnt = endPos;
        // zero 수 구하기
        var zeroCnt = maxCnt - bitCnt;
        
        // 첫번째 bit array의 bit 수 구하기
        // 총 자릿수(maxCnt)의 자리는 무조건 1
        var firstBitCnt = (maxCnt > 15) ? maxCnt % 16 : 0;
        // var firstBitCnt = (bitCnt > 15) ? maxCnt % 16 : bitCnt;
        // 마지막 bit array의 bit 수 구하기
        var lastBitCnt = (bitCnt - firstBitCnt) % 16;

        // console.log(maxCnt, bitCnt, zeroCnt, firstBitCnt, lastBitCnt);

        // bit Array 구하기
        var bitArr = [];

        if (maxCnt > 16) {
            // 첫번째 bit array 채우기
            if (firstBitCnt > 0) {
                bitArr.push(Math.pow(2, firstBitCnt)-1);
            }
            // console.log("bitArr1", bitArr);

            // 중간 bit array들 채우기
            for (var i=0; i<Math.floor((bitCnt-firstBitCnt-lastBitCnt)/16); i++)
                bitArr.push(Math.pow(2, 16)-1);
            // console.log("bitArr2", bitArr);

            // 마지막 bit array 채우기
            if (lastBitCnt > 0) {
                var lastBit = this.makeHex(0, lastBitCnt);
                bitArr.push(lastBit);
            }

            // 마지막 zero array 채우기
            for (var i=0; i<Math.floor(zeroCnt/16); i++)
                bitArr.push(0x0000);
        } else {
            var smallBit = Math.pow(2, bitCnt) - 1;
            if (this.BITNO(smallBit) < maxCnt) {
                smallBit = smallBit << (maxCnt-this.BITNO(smallBit));
            }
            bitArr.push(smallBit);
        }
        // console.log("bitArr3", bitArr);
        
        
        return [calcLeft+"%", calcWidth+"%", bitArr];
    }

    setViewItems (topPos, height) {
        for (var i = 0; i < this.items.length; i++) {
            var key = Object.keys(this.items[i])[0];
            // calculate ==> timelineScheduler4.js createItems();
            // console.log(i, key, this.items[i][key]);
            if (this.projects[i].id == key) {
                // console.log(key);
                var res = this.setItemDayPosition(this.projects[i].startDate, this.projects[i].dueDate);
                this.projects[i].left = res[0];
                this.projects[i].width = res[1];
                this.projects[i].dayBitArr = res[2];
            } else {
                console.log("could not find project id", key);
                break;
            }
            // project item 추가
            for (var j = 0; j < this.items[i][key].length; j++) {
                // console.log(i, key, j, this.items[i][key][j]);
                var res = this.setItemDayPosition(this.items[i][key][j].start, this.items[i][key][j].due);
                this.items[i][key][j].left = res[0];
                this.items[i][key][j].width = res[1];
                this.items[i][key][j].dayBitArr = res[2];
            }
        }
        console.log(this.projects, this.items);
    }

    makeNewRow () {
        var rowLength = Math.floor(this.days / 16);
        if (this.days % 16 > 0)
            rowLength++;
        var dummyRow = [];
        for (var i=0; i<rowLength; i++)
            dummyRow.push(0x0000);
        this.rowArray.push(dummyRow);
    }

    canISitOnHere (row, prjidx, itemidx, prjid, itemtype) {
        var itemBitInfo = this.items[prjidx][prjid][itemidx].dayBitArr;
        var rowBitInfo = this.rowArray[row];
        var bCan = true;
        for (var i=0; i<itemBitInfo.length; i++) {
            var bitInfoDistance = rowBitInfo.length-itemBitInfo.length;
            var d = rowBitInfo[i+bitInfoDistance] & itemBitInfo[i];
            // console.log(i, rowBitInfo[i+bitInfoDistance].toString(16), itemBitInfo[i].toString(16), d.toString(16));
            if (d != 0x0000) {
                // console.log("fail2");
                bCan = false;
                break;
            }
        }
        return bCan;
    }

    setRowNum () {
        for (var i=0; i<this.items.length; i++) {
            var key = Object.keys(this.items[i])[0];
            // console.log(key, this.projects[i].name);
            if (this.projects[i].id == key) {
                this.projects[i].row = this.rowArray.length;
                this.makeNewRow();
                this.rowArray[this.projects[i].row] = this.projects[i].dayBitArr;
            }
            
            var bSit = false;
            for (var j=0; j<this.items[i][key].length; j++) {
                // console.log(key, this.projects[i].name, this.items[i][key][j].id, this.items[i][key][j].name);
                var bAdded = false;
                var k = this.projects[i].row + 1;
                while (!bAdded) {
                    if (k >= this.rowArray.length)
                        this.makeNewRow();
                    bSit = this.canISitOnHere(k, i, j, key, 1);
                    // console.log("bSit", bSit, this.rowArray[k], this.items[i][key][j].dayBitArr);
                    if (bSit) {
                        for (var l=0; l<this.items[i][key][j].dayBitArr.length; l++) {
                            var bitInfoDistance = this.rowArray[k].length - this.items[i][key][j].dayBitArr.length;
                            var d = this.rowArray[k][l+bitInfoDistance] | this.items[i][key][j].dayBitArr[l];
                            this.rowArray[k][l+bitInfoDistance] = d;
                        }
                        this.items[i][key][j].row = k;
                        bAdded = true;
                    } else
                        k++;
                }
            }
        }
    }

    setRowNumTest () {
        var key = Object.keys(this.items[2])[0];
        if (this.projects[2].id == key) {
            this.projects[2].row = this.rowArray.length;
            // this.makeNewRow(this.rowArray.length, 0);
            this.makeNewRow();
            this.rowArray[this.projects[i].row] = this.projects[i].dayBitArr;
        }
        
        var bSit = false;
        for (var j=0; j<this.items[2][key].length; j++) {
            var bAdded = false;
            var k = this.projects[2].row + 1;
            
            while (!bAdded) {
                console.log("k & this.rowArrayLength", k, this.rowArray.length);
                if (k >= this.rowArray.length)
                    this.makeNewRow();
                    // this.makeNewRow(k, 1);
                bSit = this.canISitOnHere(k, 2, j, key, 1);
                console.log("bSit", this.rowArray[k].length);
                if (bSit) {
                    var breakTest = 0;
                    for (var l=0; l<this.items[2][key][j].dayBitArr.length; l++) {
                        if (breakTest > 100)
                            break;
                        breakTest++;
                        console.log("this.rowArray[k].length", this.rowArray[k].length);
                        var bitInfoDistance = this.rowArray[k].length - this.items[2][key][j].dayBitArr.length;
                        console.log("l, bitInfoDistance", l, bitInfoDistance);
                        console.log("bit info", this.rowArray[k][l+bitInfoDistance], this.items[2][key][j].dayBitArr[l]);
                        var d = this.rowArray[k][l+bitInfoDistance] | this.items[2][key][j].dayBitArr[l];
                        console.log("this.rowArray[k]", this.rowArray[k].toString(16));
                        this.rowArray[k][l+bitInfoDistance] = d;
                    }
                    this.items[2][key][j].row = k;
                    bAdded = true;
                    console.log('bAdded', this.rowArray);
                } else {
                    k++;
                }
            }
        }
    }

    isInHere (rowStart, rowDistance, prjid, prjkey, itemid, type) {
        var rowNo = -1;
        if (type == 0) {
            rowNo = this.projects[prjid].row;
        } else {
            rowNo = this.items[prjid][prjkey][itemid].row
        }

        if (rowNo >= rowStart && rowNo <= rowDistance)
            return true;
        else 
            return false;
    }

    getViewItems (topPos, height) {
        var rowCnt = 0;
        var destTopRow = Math.floor(topPos / this.itemHeight);  // top row는 소숫점 이하 버림
        var destTotRow = Math.ceil((topPos + height) / this.itemHeight);   // total row는 소숫점 이하 올림
        // console.log("destTopRow:", destTopRow, "destTotRow:", destTotRow);
        var calcRow = 0;

        var resArray = [];

        for (var i=0; i < this.items.length; i++) {
            if (this.projects[i].row < destTopRow || this.projects[i].row > destTotRow) {
                // console.log(i, destTopRow, destTotRow, this.projects[i].row);
                continue;
            }

            var key = Object.keys(this.items[i])[0];

            var bIsIn = false;
            if (this.projects[i].id == key) {
                bIsIn = this.isInHere(destTopRow, destTotRow, i, key, -1, 0);
                if (bIsIn) {
                    var itemArr = {};
                    itemArr.id = this.projects[i].id;
                    itemArr.name = this.projects[i].name;
                    itemArr.start = this.projects[i].startDate;
                    itemArr.due = this.projects[i].dueDate;
                    itemArr.bidtime = -1;
                    itemArr.stat = -1;
                    itemArr.left = this.projects[i].left;
                    itemArr.width = this.projects[i].width;
                    itemArr.dayBitArr = this.projects[i].dayBitArr;
                    itemArr.row = this.projects[i].row;
                    itemArr.type = 0;
                    resArray.push(itemArr);
                }
            }
            
            for (var j=0; j<this.items[i][key].length; j++) {
                bIsIn = false;
                bIsIn = this.isInHere(destTopRow, destTotRow, i, key, j, 1);
                if (bIsIn) {
                    var itemArr = {};
                    itemArr = this.items[i][key][j];
                    itemArr.type = 1;
                    resArray.push(itemArr);
                }
            }
        }
        return resArray;
    }

    // object 네 array가 있을 경우 deep clone 되지 않아 삭제
    // cloneObject(obj) {
    //     var clone = {};
    //     for(var i in obj) {
    //         if(typeof(obj[i])=="object" && obj[i] != null)
    //             clone[i] = this.cloneObject(obj[i]);
    //         else
    //             clone[i] = obj[i];
    //     }
    //     return clone;
    // }

    sortItemsByStart(prjId) {
        var res = this.getItemOfPrjId(prjId);
        var dummyItems = {};
        dummyItems = $.extend(true, {}, res[1]);

        dummyItems[prjId].sort(function (a, b) {
            return a.start < b.start ? -1 : a.start > b.start ? 1 : 0;
        });
        for (var i=0; i<this.items.length; i++) {
            var key = Object.keys(this.items[i])[0];
            if (key == "109793") {
                // this.items[i] = dummyItems;
                this.items[i] = $.extend(true, {}, dummyItems);
                break;
            }
        }
        return dummyItems;
    }

    Init () {
        console.log("Init");
        // console.log(this.items["112275"]);
        // var res = this.getItemOfPrjId("112275");
        // console.log(res[0], res[1]);
        // var res = this.getViewItems(504);
        // console.log(res);
        // this.items["112275"].sort(function (b, a) {
        //     return a.start < b.start ? -1 : a.start > b.start ? 1 : 0;
        // });
        // console.log(this.items["112275"]);
        console.log(moment().format("YYYY-MM-DD HH:mm:ss.SSS"));
        this.setViewItems();
        console.log(moment().format("YYYY-MM-DD HH:mm:ss.SSS"));
        this.setRowNum();
        // this.setRowNumTest();
        console.log(moment().format("YYYY-MM-DD HH:mm:ss.SSS"));
    }
}
