/*!  Copyright (c) 2013 Zallist

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
*/
/*! Project location: https://github.com/Zallist/TimeScheduler */

// Visual Studio references

/// <reference path="jquery-1.9.1.min.js" />
/// <reference path="jquery-ui-1.10.2.custom.min.js" />
/// <reference path="moment.min.js" />

var TimeScheduler = {
    Options: {
        /* The function to call to fill up Sections.
           Sections are cached. To clear cache, use TimelineScheduler.FillSections(true);
           Callback accepts an array of sections in the format {
            id: num,
            name: string
           }
        */
        GetSections: function (callback) { },

        /* The function to call to fill up Items.
           Callback accepts an array of items in the format 
           {
                id: num,
                name: string,
                sectionID: ID of Section,
                start: Moment of the start,
                end: Moment of the end,
                classes: string of classes to add,
                events: [
                    {
                        label: string to show in tooltip,
                        at: Moment of event,
                        classes: string of classes to add
                    }
                ]
            }
        */
        GetSchedule: function (callback, start, end) { },

        /* The Moment to start the calendar at. RECOMMENDED: .startOf('day') */
        Start: moment(),

        /* The Moment format to use when displaying Header information */
        HeaderFormat: 'Do MMM YYYY',

        /* The Moment format to use when displaying Tooltip information */
        LowerFormat: 'DD-MMM-YYYY HH:mm',

        /* An array of Periods to be selectable by the user in the form of {
            Name: unique string name to be used when selecting,
        Label: string to display on the Period Button,
            TimeframePeriod: number of minutes between intervals on the scheduler,
            TimeframeOverall: number of minutes between the Start of the period and the End of the period,
            TimeframeHeaderFormats: Array of formats to use for headers.
        }
        */
        Periods: [
            {
                Name: '2 days',
                Label: '2 days',
                TimeframePeriod: 120,
                TimeframeOverall: 2880,
                TimeframeHeaders: [
                    'Do MMM',
                    'HH'
                ],
                Classes: 'time-sch-period-2day'
            },
            {
                Name: '2 weeks',
                Label: '2 weeks',
                TimeframePeriod: 1440,
                TimeframeOverall: 20160,
                TimeframeHeaders: [
                    'MMM',
                    'Do'
                ],
                Classes: 'time-sch-period-2week'
            }
        ],

        /* The Name of the period to select */
        SelectedPeriod: '2 weeks',

        /* The Element to put the scheduler on */
        Element: $('<div></div>'),

        /* The minimum height of each section */
        MinRowHeight: 22,
        
        /* Whether to show the Current Time or not */
        ShowCurrentTime: true,

        /* Whether to show the Goto button */
        ShowGoto: true,

        /* Whether to show the Today button */
        ShowToday: true,

        /* Text to use when creating the scheduler */
        Text: {
            NextButton: 'Next',
            NextButtonTitle: 'Next period',
            PrevButton: 'Prev',
            PrevButtonTitle: 'Previous period',
            TodayButton: 'Today',
            TodayButtonTitle: 'Go to today',
            GotoButton: 'Go to',
            GotoButtonTitle: 'Go to specific date'
        },

        Events: {
            // function (item) { }
            ItemMouseEnter: null,

            // function (item) { }
            ItemMouseLeave: null,

            // function (item) { }
            ItemClicked: null,

            // function (item, sectionID, start, end) { }
            ItemDropped: null,

            // function (item, start, end) { }
            ItemResized: null,

            // function (item, start, end) { }
            // Called when any item move event is triggered (draggable.drag, resizable.resize)
            ItemMovement: null,
            // Called when any item move event starts (draggable.start, resizable.start)
            ItemMovementStart: null,
            // Called when any item move event ends (draggable.end, resizable.end)
            ItemMovementEnd: null,

            // function (eventData, itemData)
            ItemEventClicked: null,

            // function (eventData, itemData)
            ItemEventMouseEnter: null,

            // function (eventData, itemData)
            ItemEventMouseLeave: null
        },

        // Should dragging be enabled?
        AllowDragging: false,

        // Should resizing be enabled?
        AllowResizing: false,

        // Disable items on moving?
        DisableOnMove: true,

        // A given max height for the calendar, if unspecified, will expand forever
        MaxHeight: null
    },

    Wrapper: null,

    HeaderWrap: null,
    TableWrap: null,

    ContentHeaderWrap: null,
    ContentWrap: null,

    TableHeader: null,
    TableContent: null,
    SectionWrap: null,

    Table: null,
    Sections: {},
    sections2: [],
    items2: [],

    CachedSectionResult: null,
    CachedScheduleResult: null,

    SetupPrototypes: function () {
        moment.fn.tsAdd = function (input, val) {
            var dur;
            // switch args to support add('s', 1) and add(1, 's')
            if (typeof input === 'string') {
                dur = moment.duration(+val, input);
            } else {
                dur = moment.duration(input, val);
            }
            this.tsAddOrSubtractDurationFromMoment(this, dur, 1);
            return this;
        }

        moment.fn.tsSubtract = function (input, val) {
            var dur;
            // switch args to support subtract('s', 1) and subtract(1, 's')
            if (typeof input === 'string') {
                dur = moment.duration(+val, input);
            } else {
                dur = moment.duration(input, val);
            }
            this.tsAddOrSubtractDurationFromMoment(this, dur, -1);
            return this;
        }

        // Replace the AddOrSubtract function so that zoning is not taken into account at all
        moment.fn.tsAddOrSubtractDurationFromMoment = function (mom, duration, isAdding) {
            var ms = duration._milliseconds,
                d = duration._days,
                M = duration._months,
                currentDate;

            if (ms) {
                mom.milliseconds(mom.milliseconds() + ms * isAdding);
                //mom._d.setTime(+mom + ms * isAdding);
            }
            if (d) {
                mom.date(mom.date() + d * isAdding);
            }
            if (M) {
                currentDate = mom.date();
                mom.date(1)
                    .month(mom.month() + M * isAdding)
                    .date(Math.min(currentDate, mom.daysInMonth()));
            }
        }
    },

    /* Initializes the Timeline Scheduler with the given opts. If omitted, defaults are used. */
    /* This should be used to recreate the scheduler with new defaults or refill items */
    Init: function (overrideCache) {
        TimeScheduler.SetupPrototypes();

        // var period = TimeScheduler.GetSelectedPeriod();
        // if (period.Name == '3 days') {
        //     TimeScheduler.Options.Start = moment(today).add('day', -1);
        // }
        // else if (period.Name == '1 week') {
        //     TimeScheduler.Options.Start = moment(today).add('day', -3);
        // }
        // else if (period.Name == '1 month') {
        //     TimeScheduler.Options.Start = moment(today).add('day', -13);
        // }
        // else if (period.Name == '1 quarter') {
        //     console.log('today', today);
        //     var tmpDay = moment(today);
        //     TimeScheduler.Options.Start = moment(tmpDay.startOf('week')).add('week', -5);
        // }
        // else if (period.Name == '1 year') {
        //     console.log('today2', today);
        //     var tmpDay = moment(today);
        //     // TimeScheduler.Options.Start = moment(tmpDay.startOf('month')).add('month', -5);
        //     TimeScheduler.Options.Start = moment(today).add('month', -5);
        // }
        // console.log('period', period);
        TimeScheduler.Options.Start = moment(TimeScheduler.Options.Start);

        TimeScheduler.Options.Element.find('.ui-draggable').draggable('destroy');
        TimeScheduler.Options.Element.empty();

        TimeScheduler.Wrapper = $(document.createElement('div'))
            .addClass('time-sch-wrapper')
            .appendTo(TimeScheduler.Options.Element);

        TimeScheduler.HeaderWrap = $(document.createElement('div'))
            .addClass('time-sch-header-wrapper time-sch-clearfix')
            .appendTo(TimeScheduler.Wrapper);

        TimeScheduler.TableWrap = $(document.createElement('div'))
            .addClass('time-sch-table-wrapper')
            .appendTo(TimeScheduler.Wrapper);
        
        // console.log('Init');
        TimeScheduler.CreateCalendar();
        TimeScheduler.FillSections(overrideCache);
        // TimeScheduler.FillSections2(overrideCache);
        // TimeScheduler.SetTopPositionWithSort();
    },

    SetTopPositionWithSort: function () {
        var inSection = {};
        sections = TimeScheduler.sections2;
        items = TimeScheduler.items2;
        console.log(sections, items);
        for (var section in sections) {
            console.log(sections[section]);
            // inSection[item.sectionID].splice(foundPos, 0, item);
        }

        for (i=0; i<items.length; i++) {
            var item = items[i];
            console.log(item.sectionID);
            if (!inSection[item.sectionID]) {
                inSection[item.sectionID] = [];
            }
            // console.log([item.sectionID].length);
            for (var pos = 0; pos < inSection[item.sectionID].length; pos++) {
                // console.log(inSection);
                if (inSection[item.sectionID][pos].start > item.start) {
                    console.log(foundPos, pos);
                    foundPos = pos;
                    break;
                }
            }

            if (foundPos === null) {
                foundPos = inSection[item.sectionID].length;
            }

            var foundPos = inSection[item.sectionID].length;
            inSection[item.sectionID].splice(foundPos, 0, item);
            console.log(foundPos, inSection[item.sectionID].length, inSection);
        }
    },

    GetSelectedPeriod: function () {
        var period;

        for (var i = 0; i < TimeScheduler.Options.Periods.length; i++) {
            if (TimeScheduler.Options.Periods[i].Name === TimeScheduler.Options.SelectedPeriod) {
                period = TimeScheduler.Options.Periods[i];
                break;
            }
        }

        if (!period) {
            period = TimeScheduler.Options.Periods[0];
            TimeScheduler.SelectPeriod(period.Name);
        }

        return period;
    },

    GetEndOfPeriod: function (start, period) {
        return moment(start).tsAdd('days', period.TimeframeOverall);
    },

    AddHeaderClasses: function (td, columnCount, specificHeader) {
        var trs, trArray, tr;
        var tdArray, foundTD;
        var prevIndex, nextIndex, colspan;
        var complete, isEven;
        
        trs = TimeScheduler.TableHeader.find('tr');

        if (specificHeader !== undefined) {
            trs = $(trs.get(specificHeader));
        }

        if (trs.length && trs.length > 0) {
            trArray = $.makeArray(trs);

            for (var trCount = 0; trCount < trArray.length; trCount++) {
                complete = false;
                nextIndex = 0;
                tr = $(trArray[trCount]);
                tdArray = $.makeArray(tr.find('.time-sch-date-header'));

                for (var tdCount = 0; tdCount < tdArray.length && !complete; tdCount++) {
                    foundTD = $(tdArray[tdCount]);

                    colspan = Number(foundTD.attr('colspan'));
                    if (colspan && !isNaN(colspan) && colspan > 0) {
                        prevIndex = (nextIndex ? nextIndex : 0);
                        nextIndex = prevIndex + colspan;
                    }
                    else {
                        prevIndex = (nextIndex ? nextIndex : 0);
                        nextIndex = prevIndex + 1;
                    }

                    if (prevIndex === columnCount) {
                        td.addClass('time-sch-header-' + trCount + '-date-start');
                    }
                    if (nextIndex - 1 === columnCount) {
                        td.addClass('time-sch-header-' + trCount + '-date-end');
                    }

                    if (prevIndex <= columnCount && columnCount < nextIndex) {
                        complete = true;
                        isEven = tdCount % 2 === 0;

                        td.addClass('time-sch-header-' + trCount + '-date-column-' + tdCount)
                            .addClass('time-sch-header-' + trCount + '-date-' + (isEven ? 'even' : 'odd'));

                        if (foundTD.hasClass('time-sch-header-' + trCount + '-current-time')) {
                            td.addClass('time-sch-header-' + trCount + '-current-time');
                        }
                    }
                }
            }
        }
    },

    CreateCalendar: function () {
        // console.log('CreateCalendar');
        var tr, td, header;
        var minuteDiff, splits, period, end;
        var thisTime, prevDate, fThisTime, fPrevDate, colspan;
        var currentTimeIndex;

        colspan = 0;

        period = TimeScheduler.GetSelectedPeriod();
        end = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, period);

        minuteDiff = Math.abs(TimeScheduler.Options.Start.diff(end, 'days'));
        splits = (minuteDiff / period.TimeframePeriod);

        TimeScheduler.ContentHeaderWrap = $(document.createElement('div'))
            .addClass('time-sch-content-header-wrap')
            .appendTo(TimeScheduler.TableWrap);

        TimeScheduler.ContentWrap = $(document.createElement('div'))
            .addClass('time-sch-content-wrap')
            .appendTo(TimeScheduler.TableWrap);

        TimeScheduler.TableHeader = $(document.createElement('table'))
            .addClass('time-sch-table time-sch-table-header')
            .appendTo(TimeScheduler.ContentHeaderWrap);

        TimeScheduler.TableContent = $(document.createElement('table'))
            .addClass('time-sch-table time-sch-table-content')
            .appendTo(TimeScheduler.ContentWrap);

        TimeScheduler.SectionWrap = $(document.createElement('div'))
            .addClass('time-sch-section-wrapper')
            .appendTo(TimeScheduler.ContentWrap);

        if (period.Classes) {
            TimeScheduler.TableWrap.toggleClass(period.Classes, true);
        }

        console.log('splits', splits, minuteDiff, period);

        for (var headerCount = 0; headerCount < period.TimeframeHeaders.length; headerCount++) {
            prevDate = null;
            fPrevDate = null;

            isEven = true;
            colspan = 0;
            currentTimeIndex = 0;

            header = period.TimeframeHeaders[headerCount];

            tr = $(document.createElement('tr'))
                .addClass('time-sch-times time-sch-times-header-' + headerCount)
                .appendTo(TimeScheduler.TableHeader);

            td = $(document.createElement('td'))
                .addClass('time-sch-section time-sch-section-header')
                .appendTo(tr);

            for (var i = 0; i < splits; i++) {
                thisTime = moment(TimeScheduler.Options.Start)
                    .tsAdd('days', (i * period.TimeframePeriod));

                fThisTime = thisTime.format(header);
                // console.log(thisTime);
                // console.log(i, fThisTime);

                if (fPrevDate !== fThisTime) {
                    // If there is no prevDate, it's the Section Header
                    if (prevDate) {
                        td.attr('colspan', colspan);
                        colspan = 0;

                        if (moment() >= prevDate && moment() < thisTime) {
                            td.addClass('time-sch-header-' + headerCount + '-current-time');
                        }
                    }

                    prevDate = thisTime;
                    fPrevDate = fThisTime;

                    td = $(document.createElement('td'))
                        .data('header-row', headerCount)
                        .data('column-count', i)
                        .data('column-is-even', isEven)
                        .addClass('time-sch-date time-sch-date-header')
                        .append(fThisTime)
                        .appendTo(tr);
                    
                    td  .addClass('time-sch-header-' + headerCount + '-date-start')
                        .addClass('time-sch-header-' + headerCount + '-date-end')
                        .addClass('time-sch-header-' + headerCount + '-date-column-' + currentTimeIndex)
                        .addClass('time-sch-header-' + headerCount + '-date-' + ((currentTimeIndex % 2 === 0) ? 'even' : 'odd'));

                    for (var prevHeader = 0; prevHeader < headerCount; prevHeader++) {
                        TimeScheduler.AddHeaderClasses(td, i, prevHeader);
                    }

                    currentTimeIndex += 1;
                }

                colspan += 1;
            }

            td.attr('colspan', colspan);
        }

        TimeScheduler.FillHeader();
    },

    CreateSections: function (sections) {
        var timeCount, tr, td, sectionContainer, headers, i;
        console.log("CreateSections");
        // console.log(sections);

        timeCount = 1;
        headers = $.makeArray(TimeScheduler.TableHeader.find('tr'));

        for (i = 0; i < headers.length; i++) {
            if (timeCount < $(headers[i]).find('.time-sch-date-header').length) {
                timeCount = $(headers[i]).find('.time-sch-date-header').length;
            }
        }

        for (i = 0; i < sections.length; i++) {
            tr = $(document.createElement('tr'))
                .addClass('time-sch-section-row')
                .addClass(i % 2 === 0 ? 'time-sch-section-even' : 'time-sch-section-odd')
                .css('height', TimeScheduler.Options.MinRowHeight)
                .appendTo(TimeScheduler.TableContent);

            sectionContainer = $(document.createElement('div'))
                .addClass('time-sch-section-container')
                .css('height', TimeScheduler.Options.MinRowHeight)
                .data('section', sections[i])
                .appendTo(TimeScheduler.SectionWrap);

            td = $(document.createElement('td'))
                .addClass('time-sch-section time-sch-section-content')
                .data('section', sections[i])
                .append(sections[i].name)
                .appendTo(tr);
            // console.log(sections[i].name);

            // 200220, khahn, decrease td count -> todo: should be instead to bg image
            // for (time = 0; time < timeCount; time++) {
            //     td = $(document.createElement('td'))
            //         .addClass('time-sch-date time-sch-date-content')
            //         .appendTo(tr);

            //     TimeScheduler.AddHeaderClasses(td, time);
            // }

            td = $(document.createElement('td'))
                .addClass('time-sch-date time-sch-date-content')
                .appendTo(tr);

            TimeScheduler.AddHeaderClasses(td, 0);

            TimeScheduler.Sections[sections[i].id] = {
                row: tr,
                container: sectionContainer
            };
        }

        TimeScheduler.SectionWrap.css({
            left: TimeScheduler.Options.Element.find('.time-sch-section').outerWidth()
        });

        if (TimeScheduler.Options.ShowCurrentTime) {
            TimeScheduler.ShowCurrentTime();
        }
    },

    countWeekdayOccurrencesInMonth: function (date) {

        var m = moment(date),
                weekDay = m.day(),
                yearDay = m.dayOfYear(),
                count = 0;

        m.startOf('month');
        while (m.dayOfYear() <= yearDay) { 
            if (m.day() == weekDay) {
                count++; 
            }
            m.add('days', 1); 
        }

        return count;
    },

    ShowCurrentTimeHandle: null,
    ShowCurrentTime: function () {
        var currentTime, currentTimeElem, minuteDiff, currentDiff, end;

        // Stop any other timeouts happening
        if (TimeScheduler.ShowCurrentTimeHandle) {
            clearTimeout(TimeScheduler.ShowCurrentTimeHandle);
        }

        currentTime = moment();
        end = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, TimeScheduler.GetSelectedPeriod());
        minuteDiff = Math.abs(TimeScheduler.Options.Start.diff(end, 'days'));
        currentDiff = Math.abs(TimeScheduler.Options.Start.diff(currentTime, 'days'));

        currentTimeElem = TimeScheduler.Options.Element.find('.time-sch-current-time');
        currentTimeElem.remove();

        if (currentTime >= TimeScheduler.Options.Start && currentTime <= end) {
            currentTimeElem = $(document.createElement('div'))
                .addClass('time-sch-current-time')
                .css('left', ((currentDiff / minuteDiff) * 100) + '%')
                .attr('title', currentTime.format(TimeScheduler.Options.LowerFormat))
                .appendTo(TimeScheduler.SectionWrap);
        }

        // Since we're only comparing minutes, we may as well only check once every 30 seconds
        TimeScheduler.ShowCurrentTimeHandle = setTimeout(TimeScheduler.ShowCurrentTime, 30000);
    },
    // ShowCurrentTime: function () {
    //     var currentTime, currentTimeElem, minuteDiff, currentDiff, end;

    //     // Stop any other timeouts happening
    //     if (TimeScheduler.ShowCurrentTimeHandle) {
    //         clearTimeout(TimeScheduler.ShowCurrentTimeHandle);
    //     }

    //     console.log('Start:', TimeScheduler.Options.Start);

    //     // 200221, khahn, todo: shoudl be change from current time per all day to current time per current month
    //     var period = TimeScheduler.GetSelectedPeriod();
    //     if (period.Name == '1 year') {
    //         currentTime = moment();
    //         end = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, TimeScheduler.GetSelectedPeriod());

    //         currentTimeHeaderElem = TimeScheduler.Options.Element.find('.time-sch-header-1-current-time');
    //         // console.log(currentTimeHeaderElem[0].offsetLeft, currentTimeHeaderElem[0].offsetWidth);
    //         console.log('currentTimeHeaderElem[0]:', currentTimeHeaderElem[0]);

    //         console.log(moment().startOf('month'), currentTime, moment().startOf('month').diff(currentTime, "minutes"));
    //         var curMinOfMonth = currentTime.diff(moment().startOf('month'));
    //         var totMinOfMonth = moment().endOf('month').diff(moment().startOf('month'));
    //         // console.log($('.time-sch-wrapper .time-sch-section').css('width'));
    //         // console.log(parseInt($('.time-sch-wrapper .time-sch-section').css('width')));
    //         var sectionHeaderWidth = parseInt($('.time-sch-wrapper .time-sch-section').css('width'));
    //         var curMinPos = currentTimeHeaderElem[0].offsetLeft + (curMinOfMonth * 104 / totMinOfMonth) - sectionHeaderWidth;
    //         console.log(curMinOfMonth, totMinOfMonth, curMinPos);

    //         currentTimeElem = TimeScheduler.Options.Element.find('.time-sch-current-time');
    //         currentTimeElem.remove();
    //         // console.log(TimeScheduler.Options.Start, currentTime, end, minuteDiff, currentDiff)

    //         if (currentTime >= TimeScheduler.Options.Start && currentTime <= end) {
    //             currentTimeElem = $(document.createElement('div'))
    //                 .addClass('time-sch-current-time')
    //                 .css('left', curMinPos+'px')
    //                 .attr('title', currentTime.format(TimeScheduler.Options.LowerFormat))
    //                 .appendTo(TimeScheduler.SectionWrap);
    //             // console.log(currentTimeElem);
    //         }
    //     } else {
    //         currentTime = moment();
    //         end = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, TimeScheduler.GetSelectedPeriod());
    //         minuteDiff = Math.abs(TimeScheduler.Options.Start.diff(end, 'minutes'));
    //         currentDiff = Math.abs(TimeScheduler.Options.Start.diff(currentTime, 'minutes'));

    //         currentTimeElem = TimeScheduler.Options.Element.find('.time-sch-current-time');
    //         currentTimeElem.remove();
    //         // console.log(TimeScheduler.Options.Start, currentTime, end, minuteDiff, currentDiff)

    //         if (currentTime >= TimeScheduler.Options.Start && currentTime <= end) {
    //             currentTimeElem = $(document.createElement('div'))
    //                 .addClass('time-sch-current-time')
    //                 .css('left', ((currentDiff / minuteDiff) * 100) + '%')
    //                 .attr('title', currentTime.format(TimeScheduler.Options.LowerFormat))
    //                 .appendTo(TimeScheduler.SectionWrap);
    //         }
    //     }

    //     // Since we're only comparing minutes, we may as well only check once every 30 seconds
    //     TimeScheduler.ShowCurrentTimeHandle = setTimeout(TimeScheduler.ShowCurrentTime, 30000);
    // },

    makeElement: function (tsItem, minuteDiff, end, top) {
        var section = TimeScheduler.Sections[tsItem.sectionID];
        var itemDiff = tsItem.startCell;
        var itemSelfDiff = tsItem.endCell - itemDiff;
        var tsSection = TimeScheduler.Sections[tsItem.sectionID];

        var calcTop = top * 18;
        var calcLeft = (itemDiff / minuteDiff) * 100;
        var calcWidth = (itemSelfDiff / minuteDiff) * 100;

        itemElem = $(document.createElement('div'))
            .addClass('time-sch-item ' + (tsItem.classes ? tsItem.classes : ''))
            .attr('id', tsItem.id)
            .attr('startCell', itemDiff)
            .attr('endCell', itemDiff+itemSelfDiff)
            .attr('top', calcTop)
            .css({
                top: calcTop,
                left: calcLeft + '%',
                width: calcWidth + '%',
                height: '17px'
            })
            .appendTo(tsSection.container);

        itemContent = $(document.createElement('div'))
            .addClass('time-sch-item-content')
            .appendTo(itemElem);

        // item 이름 출력
        if (tsItem.name) {
            $(document.createElement('div'))
                .append(tsItem.name)
                .appendTo(itemContent);
        }
        // item에 start/end icon 표시
        if (tsItem.start >= TimeScheduler.Options.Start) {
            $(document.createElement('div'))
                .addClass('time-sch-item-start')
                .appendTo(itemElem);
        }
        if (tsItem.end <= end) {
            $(document.createElement('div'))
                .addClass('time-sch-item-end')
                .appendTo(itemElem);
        }

        tsItem.Element = itemElem;

        // // sort: tsItem이 삽입될 위치 찾기
        // // Place this in the current section array in its sorted position
        // // console.log(inSection[tsItem.sectionID].length);
        // for (var pos = 0; pos < inSection[tsItem.sectionID].length; pos++) {
        //     if (inSection[tsItem.sectionID][pos].start > tsItem.start) {
        //         foundPos = pos;
        //         break;
        //     }
        // }

        itemElem.data('item', tsItem);

        // var elemBottom = tsItem.top + tsItem.Element.outerHeight() + 1;
        // var elemBottom = tsItem.top + 17 + 1;
        var elemBottom = calcTop + 18;
        // if (calcTop == 0)
        //     console.log(tsItem);

        if (elemBottom > section.container.height()) {
            section.container.css('height', elemBottom);
            section.row.css('height', elemBottom);
        }
    },



    CreateItems: function (items) {
        var tsItem, tsSection, itemElem, itemContent;
        var startTime = TimeScheduler.Options.Start;
        var endTime = moment(startTime.format("YYYY-MM-DD")).add('month', 3).add('day', -1);
        console.log(startTime.format("YYYY-MM-DD"));
        console.log(endTime.format("YYYY-MM-DD"));

        // 3개월 날짜 수
        // var quarterLen = Math.abs(startTime.diff(endTime, 'days')) + 1;

        // var section = [];
        // var rowArr = new Array();
        // for (i = 0; i < quarterLen; i++)
        //     rowArr[i] = '0';
        // section.push(rowArr);
        // rowArr.splice(0, rowArr.length);

        var minuteDiff, foundStart, foundEnd, itemDiff, itemSelfDiff, calcTop, calcLeft, calcWidth;
        var inSection = {}, foundPos;
        var period, end, i;
        period = TimeScheduler.GetSelectedPeriod();
        end = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, period);

        minuteDiff = Math.abs(TimeScheduler.Options.Start.diff(end, 'days'));

        var section = [];
        var rowArr = new Array();
        for (i = 0; i < minuteDiff; i++)
            rowArr[i] = '0';
        section.push(rowArr);
        rowArr.splice(0, rowArr.length);

        console.log(minuteDiff);
        console.log(moment().format("YYYY-MM-DD HH:mm:ss.SSS"));

        var oldSectionId = '';
        // items loop
        for (i=0; i < items.length; i++) {
            tsItem = items[i];
            if (oldSectionId != tsItem.sectionID && inSection[tsItem.sectionID]) {
                console.log(i, inSection);
                inSection[tsItem.sectionID].sort(function (a, b) {
                    return a.start < b.start ? -1 : a.start > b.start ? 1 : 0;
                });
            }
            oldSectionId = tsItem.sectionID;
            tsSection = TimeScheduler.Sections[tsItem.sectionID];

            // section ID 검색
            if (tsSection) {
                // section ID 검색 시 inSection array 초기화
                if (!inSection[tsItem.sectionID]) {
                    inSection[tsItem.sectionID] = [];
                }

                if (tsItem.start <= end && tsItem.end >= TimeScheduler.Options.Start) {
                    foundPos = null;

                    // item 시작일/종료일 확인 후 시작/종료 위치 찾기
                    foundStart = moment(Math.max(tsItem.start, TimeScheduler.Options.Start));
                    foundEnd = moment(Math.min(tsItem.end, end));

                    itemDiff = foundStart.diff(TimeScheduler.Options.Start, 'days');
                    itemSelfDiff = Math.abs(foundStart.diff(foundEnd, 'days'));

                    calcTop = 0;
                    calcLeft = (itemDiff / minuteDiff) * 100;
                    calcWidth = (itemSelfDiff / minuteDiff) * 100;
                    // if (i==0) {
                    //     console.log(foundStart.format("YYYY-MM-DD"), foundEnd.format("YYYY-MM-DD"), itemDiff, itemSelfDiff);
                    //     console.log(calcLeft, calcWidth);
                    // }

                    items[i].startCell = itemDiff;
                    items[i].endCell = itemDiff + itemSelfDiff;
                    
                    // itemElem = $(document.createElement('div'))
                    //     .addClass('time-sch-item ' + (tsItem.classes ? tsItem.classes : ''))
                    //     .attr('id', tsItem.id)
                    //     // .attr('startCell', itemDiff)
                    //     // .attr('endCell', itemDiff+itemSelfDiff)
                    //     .css({
                    //         top: calcTop,
                    //         left: calcLeft + '%',
                    //         width: calcWidth + '%',
                    //         height: '17px'
                    //     })
                    //     .appendTo(tsSection.container);

                    // itemContent = $(document.createElement('div'))
                    //     .addClass('time-sch-item-content')
                    //     .appendTo(itemElem);

                    // // item 이름 출력
                    // if (tsItem.name) {
                    //     $(document.createElement('div'))
                    //         .append(tsItem.name)
                    //         .appendTo(itemContent);
                    // }
                    // // item에 start/end icon 표시
                    // if (tsItem.start >= TimeScheduler.Options.Start) {
                    //     $(document.createElement('div'))
                    //         .addClass('time-sch-item-start')
                    //         .appendTo(itemElem);
                    // }
                    // if (tsItem.end <= end) {
                    //     $(document.createElement('div'))
                    //         .addClass('time-sch-item-end')
                    //         .appendTo(itemElem);
                    // }

                    // tsItem.Element = itemElem;

                    // // // sort: tsItem이 삽입될 위치 찾기
                    // // // Place this in the current section array in its sorted position
                    // // // console.log(inSection[tsItem.sectionID].length);
                    // // for (var pos = 0; pos < inSection[tsItem.sectionID].length; pos++) {
                    // //     if (inSection[tsItem.sectionID][pos].start > tsItem.start) {
                    // //         foundPos = pos;
                    // //         break;
                    // //     }
                    // // }

                    // // tsItem이 삽입될 위치를 찾지 못했다면 맨 마지막으로 지정
                    // if (foundPos === null) {
                        foundPos = inSection[tsItem.sectionID].length;
                    // }

                    // // inSection array에 tsItem 삽입
                    inSection[tsItem.sectionID].splice(foundPos, 0, tsItem);

                    // itemElem.data('item', tsItem);
                }
            }
        }
        inSection[tsItem.sectionID].sort(function (a, b) {
            return a.start < b.start ? -1 : a.start > b.start ? 1 : 0;
        });
        console.log(moment().format("YYYY-MM-DD HH:mm:ss.SSS"));
        console.log("inSection length:", inSection.length);
        console.log(inSection);

        var newRowCnt = 0;
        for (var prop in inSection) {
            tsSection = TimeScheduler.Sections[prop];
            console.log(prop, TimeScheduler.Sections, tsSection.length, inSection[prop].length);

            console.log(section, section.length);
            console.log(moment().format("YYYY-MM-DD HH:mm:ss.SSS"));
            for (i = 0; i < inSection[prop].length; i++) {
                if (i%1000 == 0)
                    console.log("i:", i, inSection[prop][i].id);
                var sectionRowCnt = section.length;
                // console.log("inSection[", prop, "][", i, "]:", inSection[prop][i]);

                itemStart = inSection[prop][i].startCell;
                itemEnd = inSection[prop][i].endCell;

                for (j = 0; j < section.length; j++) {
                    // console.log(j);
                    var sectionRow = section[j];
                    needsNewRow = false;

                    if (inSection[prop][i].id == 13082 || inSection[prop][i].id == 13634)
                        console.log(inSection[prop][i].id, "sectionRow:", sectionRowCnt, j, section[j]);

                    for (checkIdx = itemStart; checkIdx <= itemEnd; checkIdx++) {
                        if (sectionRow[checkIdx] == '1') {
                            needsNewRow = true;
                            break;
                        }
                    }
                    if (needsNewRow) {
                        if (j < section.length - 1) {
                            continue;
                        } else {
                            if (j > 1655){
                                break;
                            }
                            var newRow = new Array();
                            for (k = 0; k < minuteDiff; k++) 
                                newRow[k] = "0";
                            for (checkIdx = itemStart; checkIdx < itemEnd; checkIdx++) {
                                newRow[checkIdx] = "1";
                            }
                            // console.log("1", inSection[prop][i].id, j);
                            if (j >= 1600)
                                TimeScheduler.makeElement(inSection[prop][i], minuteDiff, end, j-1600);

                            section.push(newRow);
                            
                            newRowCnt++;
                            break;
                        }
                    } else {
                        for (checkIdx = itemStart; checkIdx < itemEnd; checkIdx++) {
                            section[j][checkIdx] = "1";
                        }
                        // console.log("0", inSection[prop][i].id, j);
                        if (j >= 1600)
                            TimeScheduler.makeElement(inSection[prop][i], minuteDiff, end, j-1600);
                        break;
                    }
                }
            }
            console.log(moment().format("YYYY-MM-DD HH:mm:ss.SSS"));
        }

        // for (i = 0; i < items.length; i++) {
        //     // console.log(i, items[i].id);
        //     start = moment(items[i].start);
        //     due = moment(items[i].end);
            
        //     var calcStart;
        //     var calcDue;
        //     // 검색일 기준 시작일 지정
        //     if (startTime > start)
        //         calcStart = startTime;
        //     else
        //         calcStart = start;
        //     foundStart = moment(Math.max())
        //     // 검색일 기준 종료일 지정
        //     if (endTime < due)
        //         calcDue = endTime
        //     else
        //         calcDue = due;
        //     // item width
        //     var itemLength = Math.abs(calcStart.diff(calcDue, 'day')) + 1;
        //     var itemStart = calcStart.diff(startTime, 'day');
        //     if (itemStart < 0)
        //         itemStart = 0;
        //     // console.log(startTime.format("YYYY-MM-DD"), calcStart.format("YYYY-MM-DD"));

        //     var sectionRowCnt = section.length;
        //     for (j = 0; j < sectionRowCnt; j++) {
        //         // console.log('j:', j);
        //         var sectionRow = section[j];
        //         needsNewRow = false;

        //         for (checkIdx = itemStart; checkIdx < itemStart + itemLength; checkIdx++) {
        //             if (sectionRow[checkIdx] == '1') {
        //                 needsNewRow = true;
        //                 break;
        //             }
        //         }
        //         if (needsNewRow) {
        //             if (j < section.length - 1)
        //                 continue;
        //             else {
        //                 var newRow = new Array();
        //                 for (k = 0; k < quarterLen; k++)
        //                     newRow[k] = '0';
        //                 for (checkIdx = itemStart; checkIdx < itemStart + itemLength; checkIdx++)
        //                     newRow[checkIdx] = '1';

        //                 section.push(newRow);
        //                 newRow.splice(0, newRow.length);
        //                 // console.log(checkIdx, itemStart, itemLength);
        //                 // if (newRow.length == 91)
        //                 //     console.log(i, checkIdx, itemStart, itemLength, newRow.length, quarterLen);
        //             }
        //         } else {
        //             for (checkIdx = itemStart; checkIdx < itemStart + itemLength; checkIdx++)
        //                 section[j][checkIdx] = '1';
        //         }
        //         // console.log("j loop end");
        //     }
        //     // console.log("i loop end");
        // }
        
        // for (i = 0; i < section.length; i++)
        //     console.log(i, 'th', section[i].length);
        console.log(section);
        // section.splice(0, section.length);

        // var item, event, section, itemElem, eventElem, itemContent, itemName, itemIcon;
        // var minuteDiff, splits, itemDiff, itemSelfDiff, eventDiff, calcTop, calcLeft, calcWidth, foundStart, foundEnd;
        // var inSection = {}, foundPos, elem, prevElem, needsNewRow;
        // var period, end, i;

        // period = TimeScheduler.GetSelectedPeriod();
        // end = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, period);

        // minuteDiff = Math.abs(TimeScheduler.Options.Start.diff(end, 'days'));

        // console.log('items:', items, minuteDiff);

        // for (i = 0; i < items.length; i++) {
        //     // console.log(i);
        //     item = items[i];
        //     section = TimeScheduler.Sections[item.sectionID];

        //     // console.log(i, section, item);

        //     if (section) {
        //         if (!inSection[item.sectionID]) {
        //             inSection[item.sectionID] = [];
        //         }

        //         if (item.start <= end && item.end >= TimeScheduler.Options.Start) {
        //             foundPos = null;

        //             foundStart = moment(Math.max(item.start, TimeScheduler.Options.Start));
        //             foundEnd = moment(Math.min(item.end, end));

        //             itemDiff = foundStart.diff(TimeScheduler.Options.Start, 'days');
        //             itemSelfDiff = Math.abs(foundStart.diff(foundEnd, 'days'));
                    
        //             calcTop = 0;
        //             calcLeft = (itemDiff / minuteDiff) * 100;
        //             calcWidth = (itemSelfDiff / minuteDiff) * 100;
                    
        //             itemElem = $(document.createElement('div'))
        //                 .addClass('time-sch-item ' + (item.classes ? item.classes : ''))
        //                 .attr('id', item.id)
        //                 .css({
        //                     top: calcTop,
        //                     left: calcLeft + '%',
        //                     width: calcWidth + '%',
        //                     height: '17px'
        //                 })
        //                 .appendTo(section.container);

        //             itemContent = $(document.createElement('div'))
        //                 .addClass('time-sch-item-content')
        //                 .appendTo(itemElem);

        //             if (item.name) {
        //                 $(document.createElement('div'))
        //                     .append(item.name)
        //                     .appendTo(itemContent);
        //             }

        //             if (item.start >= TimeScheduler.Options.Start) {
        //                 $(document.createElement('div'))
        //                     .addClass('time-sch-item-start')
        //                     .appendTo(itemElem);
        //             }
        //             if (item.end <= end) {
        //                 $(document.createElement('div'))
        //                     .addClass('time-sch-item-end')
        //                     .appendTo(itemElem);
        //             }

        //             item.Element = itemElem;

        //             // Place this in the current section array in its sorted position
        //             for (var pos = 0; pos < inSection[item.sectionID].length; pos++) {
        //                 if (inSection[item.sectionID][pos].start > item.start) {
        //                     console.log(foundPos, pos);
        //                     foundPos = pos;
        //                     break;
        //                 }
        //             }

        //             if (foundPos === null) {
        //                 foundPos = inSection[item.sectionID].length;
        //             }
        //             // console.log(foundPos);

        //             inSection[item.sectionID].splice(foundPos, 0, item);

        //             itemElem.data('item', item);

        //             // TimeScheduler.SetupItemEvents(itemElem);
        //         }
        //     }
        // }
        
        // // Sort out layout issues so no elements overlap
        // var splitArrs = [];
        // console.log('start day:', item.start.format("YYYY-MM-DD"), moment(item.start).diff(TimeScheduler.Options.Start, 'day'));
        // console.log('end day:', item.end.format("YYYY-MM-DD"), moment(item.end).diff(TimeScheduler.Options.Start, 'day'));

        // console.log('inSection:', inSection, moment());

        // var rowCnt = 0;
        // var lastPrev = 0;
        // var prev = 0;
        // var lastPrevElem;
        // // section loop
        // for (var prop in inSection) {
        //     section = TimeScheduler.Sections[prop];
        //     console.log(prop, inSection[prop].length);
        //     // console.log('section:', section);
        //     // console.log('inSection[prop]:', inSection[prop]);

        //     // item loop
        //     for (i=0; i< inSection[prop].length; i++) {

        //     }
        //     for (i = 0; i < inSection[prop].length; i++) {
        //         var elemTop, elemBottom;
        //         elem = inSection[prop][i];
        //         console.log(i, rowCnt, lastPrev, elem);

        //         // var splitArr = [period.TimeframeOverall];
        //         // // console.log(splitArr);
        //         // for (j=0; j<period.TimeframeOverall; j++)
        //         //     splitArr[j] = 0;

        //         rowCnt = 0;
                
        //         // If we're passed the first item in the row
        //         for (prev = 0; prev < i; prev++) {
        //             // console.log(i, prev);
        //             var prevElemTop, prevElemBottom;
        //             prevElem = inSection[prop][prev];

        //             prevElemTop = prevElem.Element.position().top;
        //             prevElemBottom = prevElemTop + prevElem.Element.outerHeight();

        //             elemTop = elem.Element.position().top;
        //             elemBottom = elemTop + elem.Element.outerHeight();
                        
        //             // (elem.start must be between prevElem.start and prevElem.end OR
        //             //  elem.end must be between prevElem.start and prevElem.end) AND
        //             // (elem.top must be between prevElem.top and prevElem.bottom OR
        //             //  elem.bottom must be between prevElem.top and prevElem.bottom)
        //             needsNewRow =
        //                 (
        //                     (prevElem.start <= elem.start && elem.start <= prevElem.end) ||
        //                     (prevElem.start <= elem.end && elem.end <= prevElem.end)
        //                 ) && (
        //                     prevElem.top <= elem.top 
        //                     // (prevElemTop <= elemTop && elemTop <= prevElemBottom) ||
        //                     // (prevElemTop <= elemBottom && elemBottom <= prevElemBottom)
        //                 );
        //             // console.log(i, prev, needsNewRow, inSection[prop][i], inSection[prop][prev]);
        //             // splitArrs.push(splitArr);
        //             if (needsNewRow) {
        //                 // elem.Element.css('top', prevElemBottom + 1);
        //                 elem.Element.css('top', prevElem.top + prevElem.Element.outerHeight() + 1);
        //                 elem.top = prevElem.top + prevElem.Element.outerHeight() + 1;
        //                 rowCnt++;
        //             }

        //             if (elem.id == '15930') {
        //                 console.log(needsNewRow, rowCnt, prev, prevElem.id, elem.top, elem.start, elem.end, prevElem.top, prevElem.start, prevElem.end);
        //             }

        //             // if (rowCnt > 67) {
        //             //     elem.Element.css('display', 'none');
        //             //     break;
        //             // }
        //         }
                
        //         // if (rowCnt <= 67)
        //             elemBottom = elem.top + elem.Element.outerHeight() + 1;
        //         // elemBottom = elem.Element.position().top + elem.Element.outerHeight() + 1;

        //         if (elemBottom > section.container.height()) {
        //             section.container.css('height', elemBottom);
        //             section.row.css('height', elemBottom);
        //         }

        //         elem.Element.css('display', 'none');
        //     }
        // }
        // console.log("end of item's sorting", moment());
    },

    // CreateItems: function (items) {
    //     var item, event, section, itemElem, eventElem, itemContent, itemName, itemIcon;
    //     var minuteDiff, splits, itemDiff, itemSelfDiff, eventDiff, calcTop, calcLeft, calcWidth, foundStart, foundEnd;
    //     var inSection = {}, foundPos, elem, prevElem, needsNewRow;
    //     var period, end, i;

    //     period = TimeScheduler.GetSelectedPeriod();
    //     end = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, period);

    //     minuteDiff = Math.abs(TimeScheduler.Options.Start.diff(end, 'days'));

    //     console.log('items:', items, minuteDiff);

    //     for (i = 0; i < items.length; i++) {
    //         // console.log(i);
    //         item = items[i];
    //         section = TimeScheduler.Sections[item.sectionID];

    //         // console.log(i, section, item);

    //         if (section) {
    //             if (!inSection[item.sectionID]) {
    //                 inSection[item.sectionID] = [];
    //             }

    //             if (item.start <= end && item.end >= TimeScheduler.Options.Start) {
    //                 foundPos = null;

    //                 foundStart = moment(Math.max(item.start, TimeScheduler.Options.Start));
    //                 foundEnd = moment(Math.min(item.end, end));

    //                 itemDiff = foundStart.diff(TimeScheduler.Options.Start, 'days');
    //                 itemSelfDiff = Math.abs(foundStart.diff(foundEnd, 'days'));
                    
    //                 calcTop = 0;
    //                 calcLeft = (itemDiff / minuteDiff) * 100;
    //                 calcWidth = (itemSelfDiff / minuteDiff) * 100;
                    
    //                 itemElem = $(document.createElement('div'))
    //                     .addClass('time-sch-item ' + (item.classes ? item.classes : ''))
    //                     .attr('id', item.id)
    //                     .css({
    //                         top: calcTop,
    //                         left: calcLeft + '%',
    //                         width: calcWidth + '%',
    //                         height: '17px'
    //                     })
    //                     .appendTo(section.container);

    //                 itemContent = $(document.createElement('div'))
    //                     .addClass('time-sch-item-content')
    //                     .appendTo(itemElem);

    //                 if (item.name) {
    //                     $(document.createElement('div'))
    //                         .append(item.name)
    //                         .appendTo(itemContent);
    //                 }

    //                 // if (item.events) {
    //                 //     for (var ev = 0; ev < item.events.length; ev++) {
    //                 //         event = item.events[ev];

    //                 //         eventDiff = (event.at.diff(foundStart, 'days') / itemSelfDiff) * 100;
                            
    //                 //         $(document.createElement('div'))
    //                 //             .addClass('time-sch-item-event ' + (event.classes ? event.classes : ''))
    //                 //             .css('left', eventDiff + '%')
    //                 //             .attr('title', event.at.format(TimeScheduler.Options.LowerFormat) + ' - ' + event.label)
    //                 //             .data('event', event)
    //                 //             .appendTo(itemElem);
    //                 //     }
    //                 // }

    //                 // if (item.start >= TimeScheduler.Options.Start) {
    //                 //     $(document.createElement('div'))
    //                 //         .addClass('time-sch-item-start')
    //                 //         .appendTo(itemElem);
    //                 // }
    //                 // if (item.end <= end) {
    //                 //     $(document.createElement('div'))
    //                 //         .addClass('time-sch-item-end')
    //                 //         .appendTo(itemElem);
    //                 // }

    //                 item.Element = itemElem;

    //                 // Place this in the current section array in its sorted position
    //                 for (var pos = 0; pos < inSection[item.sectionID].length; pos++) {
    //                     if (inSection[item.sectionID][pos].start > item.start) {
    //                         console.log(foundPos, pos);
    //                         foundPos = pos;
    //                         break;
    //                     }
    //                 }

    //                 if (foundPos === null) {
    //                     foundPos = inSection[item.sectionID].length;
    //                 }
    //                 // console.log(foundPos);

    //                 inSection[item.sectionID].splice(foundPos, 0, item);

    //                 itemElem.data('item', item);

    //                 // TimeScheduler.SetupItemEvents(itemElem);
    //             }
    //         }
    //     }
        
    //     // Sort out layout issues so no elements overlap
    //     var splitArrs = [];
    //     console.log('start day:', item.start.format("YYYY-MM-DD"), moment(item.start).diff(TimeScheduler.Options.Start, 'day'));
    //     console.log('end day:', item.end.format("YYYY-MM-DD"), moment(item.end).diff(TimeScheduler.Options.Start, 'day'));

    //     console.log('inSection:', inSection, moment());

    //     var rowCnt = 0;
    //     var lastPrev = 0;
    //     var prev = 0;
    //     var lastPrevElem;
    //     // section loop
    //     for (var prop in inSection) {
    //         section = TimeScheduler.Sections[prop];
    //         console.log(prop, inSection[prop].length);
    //         // console.log('section:', section);
    //         // console.log('inSection[prop]:', inSection[prop]);

    //         // item loop
    //         for (i=0; i< inSection[prop].length; i++) {

    //         }
    //         for (i = 0; i < inSection[prop].length; i++) {
    //             var elemTop, elemBottom;
    //             elem = inSection[prop][i];
    //             console.log(i, rowCnt, lastPrev, elem);

    //             // var splitArr = [period.TimeframeOverall];
    //             // // console.log(splitArr);
    //             // for (j=0; j<period.TimeframeOverall; j++)
    //             //     splitArr[j] = 0;

    //             rowCnt = 0;
                
    //             // If we're passed the first item in the row
    //             for (prev = 0; prev < i; prev++) {
    //                 // console.log(i, prev);
    //                 var prevElemTop, prevElemBottom;
    //                 prevElem = inSection[prop][prev];

    //                 prevElemTop = prevElem.Element.position().top;
    //                 prevElemBottom = prevElemTop + prevElem.Element.outerHeight();

    //                 elemTop = elem.Element.position().top;
    //                 elemBottom = elemTop + elem.Element.outerHeight();
                        
    //                 // (elem.start must be between prevElem.start and prevElem.end OR
    //                 //  elem.end must be between prevElem.start and prevElem.end) AND
    //                 // (elem.top must be between prevElem.top and prevElem.bottom OR
    //                 //  elem.bottom must be between prevElem.top and prevElem.bottom)
    //                 needsNewRow =
    //                     (
    //                         (prevElem.start <= elem.start && elem.start <= prevElem.end) ||
    //                         (prevElem.start <= elem.end && elem.end <= prevElem.end)
    //                     ) && (
    //                         prevElem.top <= elem.top 
    //                         // (prevElemTop <= elemTop && elemTop <= prevElemBottom) ||
    //                         // (prevElemTop <= elemBottom && elemBottom <= prevElemBottom)
    //                     );
    //                 // console.log(i, prev, needsNewRow, inSection[prop][i], inSection[prop][prev]);
    //                 // splitArrs.push(splitArr);
    //                 if (needsNewRow) {
    //                     // elem.Element.css('top', prevElemBottom + 1);
    //                     elem.Element.css('top', prevElem.top + prevElem.Element.outerHeight() + 1);
    //                     elem.top = prevElem.top + prevElem.Element.outerHeight() + 1;
    //                     rowCnt++;
    //                 }

    //                 if (elem.id == '15930') {
    //                     console.log(needsNewRow, rowCnt, prev, prevElem.id, elem.top, elem.start, elem.end, prevElem.top, prevElem.start, prevElem.end);
    //                 }

    //                 // if (rowCnt > 67) {
    //                 //     elem.Element.css('display', 'none');
    //                 //     break;
    //                 // }
    //             }
                
    //             // if (rowCnt <= 67)
    //                 elemBottom = elem.top + elem.Element.outerHeight() + 1;
    //             // elemBottom = elem.Element.position().top + elem.Element.outerHeight() + 1;

    //             if (elemBottom > section.container.height()) {
    //                 section.container.css('height', elemBottom);
    //                 section.row.css('height', elemBottom);
    //             }

    //             elem.Element.css('display', 'none');
    //         }
    //     }
    //     console.log("end of item's sorting", moment());
    // },

    SetupItemEvents: function (itemElem) {
        if (TimeScheduler.Options.Events.ItemClicked) {
            itemElem.click(function (event) {
                event.preventDefault();
                TimeScheduler.Options.Events.ItemClicked.call(this, $(this).data('item'));
            });
        }

        if (TimeScheduler.Options.Events.ItemMouseEnter) {
            itemElem.mouseenter(function (event) {
                TimeScheduler.Options.Events.ItemMouseEnter.call(this, $(this).data('item'));
            });
        }

        if (TimeScheduler.Options.Events.ItemMouseLeave) {
            itemElem.mouseleave(function (event) {
                TimeScheduler.Options.Events.ItemMouseLeave.call(this, $(this).data('item'));
            });
        }

        if (TimeScheduler.Options.AllowDragging) {
            itemElem.draggable({
                helper: 'clone',
                zIndex: 1,
                appendTo: TimeScheduler.SectionWrap,
                distance: 5,
                snap: '.time-sch-section-container',
                snapMode: 'inner',
                snapTolerance: 10,
                drag: function (event, ui) {
                    var item, start, end;
                    var period, periodEnd, minuteDiff;

                    if (TimeScheduler.Options.Events.ItemMovement) {
                        period = TimeScheduler.GetSelectedPeriod();
                        periodEnd = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, period);
                        minuteDiff = Math.abs(TimeScheduler.Options.Start.diff(periodEnd, 'days'));

                        item = $(event.target).data('item');

                        start = moment(TimeScheduler.Options.Start).tsAdd('days', minuteDiff * (ui.helper.position().left / TimeScheduler.SectionWrap.width()));
                        end = moment(start).tsAdd('days', Math.abs(item.start.diff(item.end, 'days')));

                        // If the start is before the start of our calendar, add the offset
                        if (item.start < TimeScheduler.Options.Start) {
                            start.tsAdd('days', item.start.diff(TimeScheduler.Options.Start, 'days'));
                            end.tsAdd('days', item.start.diff(TimeScheduler.Options.Start, 'days'));
                        }

                        TimeScheduler.Options.Events.ItemMovement.call(this, item, start, end);
                    }
                },
                start: function (event, ui) {
                    $(this).hide();

                    // We only want content to show, not events or resizers
                    ui.helper.children().not('.time-sch-item-content').remove();

                    if (TimeScheduler.Options.Events.ItemMovementStart) {
                        TimeScheduler.Options.Events.ItemMovementStart.call(this);
                    }
                },
                stop: function (event, ui) {
                    if ($(this).length) {
                        $(this).show();
                    }

                    if (TimeScheduler.Options.Events.ItemMovementEnd) {
                        TimeScheduler.Options.Events.ItemMovementEnd.call(this);
                    }
                },
                cancel: '.time-sch-item-end, .time-sch-item-start, .time-sch-item-event'
            });

            $('.time-sch-section-container').droppable({
                greedy: true,
                hoverClass: 'time-sch-droppable-hover',
                tolerance: 'pointer',
                drop: function (event, ui) {
                    var item, sectionID, start, end;
                    var period, periodEnd, minuteDiff;

                    period = TimeScheduler.GetSelectedPeriod();
                    periodEnd = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, period);
                    minuteDiff = Math.abs(TimeScheduler.Options.Start.diff(periodEnd, 'days'));

                    item = ui.draggable.data('item');
                    sectionID = $(this).data('section').id;

                    start = moment(TimeScheduler.Options.Start).tsAdd('days', minuteDiff * (ui.helper.position().left / $(this).width()));
                    end = moment(start).tsAdd('days', Math.abs(item.start.diff(item.end, 'days')));

                    // If the start is before the start of our calendar, add the offset
                    if (item.start < TimeScheduler.Options.Start) {
                        start.tsAdd('days', item.start.diff(TimeScheduler.Options.Start, 'days'));
                        end.tsAdd('days', item.start.diff(TimeScheduler.Options.Start, 'days'));
                    }

                    // Append original to this section and reposition it while we wait
                    ui.draggable.appendTo($(this));
                    ui.draggable.css({
                        left: ui.helper.position().left - $(this).position().left,
                        top: ui.helper.position().top - $(this).position().top
                    });

                    if (TimeScheduler.Options.DisableOnMove) {
                        if (ui.draggable.data('uiDraggable')) {
                            ui.draggable.draggable('disable');
                        }
                        if (ui.draggable.data('uiResizable')) {
                            ui.draggable.resizable('disable');
                        }
                    }
                    ui.draggable.show();

                    if (TimeScheduler.Options.Events.ItemDropped) {
                        // Time for a hack, JQueryUI throws an error if the draggable is removed in a drop
                        setTimeout(function () {
                            TimeScheduler.Options.Events.ItemDropped.call(this, item, sectionID, start, end);
                        }, 0);
                    }
                }
            });
        }

        if (TimeScheduler.Options.AllowResizing) {
            var foundHandles = null;
            
            if (itemElem.find('.time-sch-item-start').length && itemElem.find('.time-sch-item-end').length) {
                foundHandles = 'e, w';
            }
            else if (itemElem.find('.time-sch-item-start').length) {
                foundHandles = 'w';
            }
            else if (itemElem.find('.time-sch-item-end').length) {
                foundHandles = 'e';
            }

            if (foundHandles) {
                itemElem.resizable({
                    handles: foundHandles,
                    resize: function (event, ui) {
                        var item, start, end;
                        var period, periodEnd, minuteDiff;

                        if (TimeScheduler.Options.Events.ItemMovement) {
                            period = TimeScheduler.GetSelectedPeriod();
                            periodEnd = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, period);
                            minuteDiff = Math.abs(TimeScheduler.Options.Start.diff(periodEnd, 'days'));

                            item = $(this).data('item');

                            if (ui.position.left !== ui.originalPosition.left) {
                                // Left handle moved

                                start = moment(TimeScheduler.Options.Start).tsAdd('days', minuteDiff * ($(this).position().left / TimeScheduler.SectionWrap.width()));
                                end = item.end;
                            }
                            else {
                                // Right handle moved

                                start = item.start;
                                end = moment(TimeScheduler.Options.Start).tsAdd('days', minuteDiff * (($(this).position().left + $(this).width()) / TimeScheduler.SectionWrap.width()));
                            }

                            TimeScheduler.Options.Events.ItemMovement.call(this, item, start, end);
                        }
                    },
                    start: function (event, ui) {
                        // We don't want any events to show
                        $(this).find('.time-sch-item-event').hide();

                        if (TimeScheduler.Options.Events.ItemMovementStart) {
                            TimeScheduler.Options.Events.ItemMovementStart.call(this);
                        }
                    },
                    stop: function (event, ui) {
                        var item, start, end;
                        var period, periodEnd, minuteDiff, section;
                        var $this;

                        $this = $(this);

                        period = TimeScheduler.GetSelectedPeriod();
                        periodEnd = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, period);
                        minuteDiff = Math.abs(TimeScheduler.Options.Start.diff(periodEnd, 'days'));

                        item = $this.data('item');

                        if (ui.position.left !== ui.originalPosition.left) {
                            // Left handle moved

                            start = moment(TimeScheduler.Options.Start).tsAdd('days', minuteDiff * ($this.position().left / TimeScheduler.SectionWrap.width()));
                            end = item.end;
                        }
                        else {
                            // Right handle moved

                            start = item.start;
                            end = moment(TimeScheduler.Options.Start).tsAdd('days', minuteDiff * (($this.position().left + $this.width()) / TimeScheduler.SectionWrap.width()));
                        }

                        if (TimeScheduler.Options.DisableOnMove) {
                            if ($this.data('uiDraggable')) {
                                $this.draggable('disable');
                            }
                            if ($this.data('uiResizable')) {
                                $this.resizable('disable');
                            }

                            $this.find('.time-sch-item-event').show();
                        }

                        if (TimeScheduler.Options.Events.ItemMovementEnd) {
                            TimeScheduler.Options.Events.ItemMovementEnd.call(this);
                        }

                        if (TimeScheduler.Options.Events.ItemResized) {
                            TimeScheduler.Options.Events.ItemResized.call(this, item, start, end);
                        }
                    }
                });
            }
        }

        if (TimeScheduler.Options.Events.ItemEventClicked) {
            itemElem.find('.time-sch-item-event').click(function (event) {
                var itemElem = $(this).closest('.time-sch-item');

                event.preventDefault();
                TimeScheduler.Options.Events.ItemEventClicked.call(this, $(this).data('event'), itemElem.data('item'));
            });
        }
        if (TimeScheduler.Options.Events.ItemEventMouseEnter) {
            itemElem.find('.time-sch-item-event').mouseenter(function (event) {
                var itemElem = $(this).closest('.time-sch-item');

                event.preventDefault();
                TimeScheduler.Options.Events.ItemEventClicked.call(this, $(this).data('event'), itemElem.data('item'));
            });
        }
        if (TimeScheduler.Options.Events.ItemEventMouseLeave) {
            itemElem.find('.time-sch-item-event').mouseleave(function (event) {
                var itemElem = $(this).closest('.time-sch-item');

                event.preventDefault();
                TimeScheduler.Options.Events.ItemEventClicked.call(this, $(this).data('event'), itemElem.data('item'));
            });
        }
    },

    FillSections2: function(override) {
        if (!TimeScheduler.CachedSectionResult || override) {
            TimeScheduler.Options.GetSections.call(this, TimeScheduler.FillSections2_Callback);
        }
        else {
            console.log('FillSections2');
            TimeScheduler.FillSections2_Callback(TimeScheduler.CachedSectionResult);
        }
    },

    FillSchedule2: function() {
        period = TimeScheduler.GetSelectedPeriod();
        end = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, period);

        TimeScheduler.Options.GetSchedule.call(this, TimeScheduler.FillSchedule2_Callback, TimeScheduler.Options.Start, end);
    },

    FillSections2_Callback: function (obj) {
        TimeScheduler.CachedSectionResult = obj;
        TimeScheduler.sections2 = obj;

        TimeScheduler.FillSchedule2();
    },

    FillSchedule2_Callback: function(obj) {
        TimeScheduler.CachedScheduleResult = obj;
        TimeScheduler.items2 = obj;
    },

    /* Call this with "true" as override, and sections will be reloaded. Otherwise, cached sections will be used */
    FillSections: function (override) {
        // console.log('FillSections');
        if (!TimeScheduler.CachedSectionResult || override) {
            console.log('FillSections1');
            TimeScheduler.Options.GetSections.call(this, TimeScheduler.FillSections_Callback);
        }
        else {
            console.log('FillSections2');
            TimeScheduler.FillSections_Callback(TimeScheduler.CachedSectionResult);
        }
    },

    FillSections_Callback: function (obj) {
        TimeScheduler.CachedSectionResult = obj;

        TimeScheduler.CreateSections(obj);
        TimeScheduler.FillSchedule();
    },

    FillSchedule: function () {
        var period, end;

        period = TimeScheduler.GetSelectedPeriod();
        end = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, period);

        TimeScheduler.Options.GetSchedule.call(this, TimeScheduler.FillSchedule_Callback, TimeScheduler.Options.Start, end);
    },

    FillSchedule_Callback: function (obj) {
        TimeScheduler.CachedScheduleResult = obj;
        TimeScheduler.CreateItems(obj);
    },

    FillHeader: function () {
        var durationString, title, periodContainer, timeContainer, periodButton, timeButton;
        var selectedPeriod, end, period;
        
        periodContainer = $(document.createElement('div'))
            .addClass('time-sch-period-container');

        timeContainer = $(document.createElement('div'))
            .addClass('time-sch-time-container');

        title = $(document.createElement('div'))
            .addClass('time-sch-title');

        TimeScheduler.HeaderWrap
            .empty()
            .append(periodContainer, timeContainer, title);

        selectedPeriod = TimeScheduler.GetSelectedPeriod();
        end = TimeScheduler.GetEndOfPeriod(TimeScheduler.Options.Start, selectedPeriod);

        // Header needs a title
        // We take away 1 minute 
        title.text(TimeScheduler.Options.Start.format(TimeScheduler.Options.HeaderFormat) + ' - ' + end.tsAdd('days', -1).format(TimeScheduler.Options.HeaderFormat));

        for (var i = 0; i < TimeScheduler.Options.Periods.length; i++) {
            period = TimeScheduler.Options.Periods[i];

            $(document.createElement('a'))
                .addClass('time-sch-period-button time-sch-button')
                .addClass(period.Name === selectedPeriod.Name ? 'time-sch-selected-button' : '')
                .attr('href', '#')
                .append(period.Label)
                .data('period', period)
                .click(TimeScheduler.Period_Clicked)
                .appendTo(periodContainer);
        }

        if (TimeScheduler.Options.ShowGoto) {
            $(document.createElement('a'))
                .addClass('time-sch-time-button time-sch-time-button-goto time-sch-button')
                .attr({
                    href: '#',
                    title: TimeScheduler.Options.Text.GotoButtonTitle
                })
                .append(TimeScheduler.Options.Text.GotoButton)
                .click(TimeScheduler.GotoTimeShift_Clicked)
                .appendTo(timeContainer);
        }

        if (TimeScheduler.Options.ShowToday) {
            $(document.createElement('a'))
                .addClass('time-sch-time-button time-sch-time-button-today time-sch-button')
                .attr({
                    href: '#',
                    title: TimeScheduler.Options.Text.TodayButtonTitle
                })
                .append(TimeScheduler.Options.Text.TodayButton)
                .click(TimeScheduler.TimeShift_Clicked)
                .appendTo(timeContainer);
        }

        $(document.createElement('a'))
            .addClass('time-sch-time-button time-sch-time-button-prev time-sch-button')
            .attr({
                href: '#',
                title: TimeScheduler.Options.Text.PrevButtonTitle
            })
            .append(TimeScheduler.Options.Text.PrevButton)
            .click(TimeScheduler.TimeShift_Clicked)
            .appendTo(timeContainer);

        $(document.createElement('a'))
            .addClass('time-sch-time-button time-sch-time-button-next time-sch-button')
            .attr({
                href: '#',
                title: TimeScheduler.Options.Text.NextButtonTitle
            })
            .append(TimeScheduler.Options.Text.NextButton)
            .click(TimeScheduler.TimeShift_Clicked)
            .appendTo(timeContainer);
    },

    GotoTimeShift_Clicked: function (event) {
        event.preventDefault();

        $(document.createElement('input'))
            .attr('type', 'text')
            .css({
                position: 'absolute',
                left: 0,
                bottom: 0
            })
            .appendTo($(this))
            .datepicker({
                onClose: function () {
                    $(this).remove();
                },
                onSelect: function (date) {
                    TimeScheduler.Options.Start = moment(date);
                    console.log('Init1');
                    TimeScheduler.Init();
                },
                defaultDate: TimeScheduler.Options.Start.toDate()
            })
            .datepicker('show')
            .hide();
    },
    TimeShift_Clicked: function (event) {
        var period;

        event.preventDefault();
        period = TimeScheduler.GetSelectedPeriod();

        console.log('this:', $(this));
        console.log('period:', period);

        if ($(this).is('.time-sch-time-button-today')) {
            TimeScheduler.Options.Start = moment().startOf('day');
        }
        else if ($(this).is('.time-sch-time-button-prev')) {
            TimeScheduler.Prev_Clicked(period);
            // TimeScheduler.Options.Start.tsAdd('minutes', period.TimeframeOverall * -1);
        }
        else if ($(this).is('.time-sch-time-button-next')) {
            TimeScheduler.Next_Clicked(period);
            // TimeScheduler.Options.Start.tsAdd('minutes', period.TimeframeOverall);
        }

        console.log('Init2');
        TimeScheduler.Init();
    },

    Prev_Clicked: function (period) {
        // if (period.Name == '3 days' || period.Name == '1 week' || period.Name == '1 month') {
        //     TimeScheduler.Options.Start.tsAdd('minutes', period.TimeframeOverall * -1);
        // }
        // else if (period.Name == '1 quarter') {
        //     console.log('today', today);
        //     var tmpDay = moment(today);
        //     TimeScheduler.Options.Start.tsAdd('week', -5);
        // }
        // else if (period.Name == '1 year') {
        //     console.log('today2', today);
        //     var tmpDay = moment(today);
        //     // TimeScheduler.Options.Start = moment(tmpDay.startOf('month')).add('month', -5);
        //     TimeScheduler.Options.Start = moment(today).add('month', -5);
        // }
        TimeScheduler.Options.Start.tsAdd('days', period.TimeframeOverall * -1);
    },

    Next_Clicked: function (period) {
        // if (period.Name == '3 days' || period.Name == '1 week' || period.Name == '1 month') {
        //     TimeScheduler.Options.Start.tsAdd('minutes', period.TimeframeOverall);
        // }
        TimeScheduler.Options.Start.tsAdd('days', period.TimeframeOverall);
    },

    /* Selects the period with the given name */
    SelectPeriod: function (name) {
        TimeScheduler.Options.SelectedPeriod = name;
        console.log('Init3');
        TimeScheduler.Init();
    },

    Period_Clicked: function (event) {
        event.preventDefault();
        TimeScheduler.SelectPeriod($(this).data('period').Name);
    }
};