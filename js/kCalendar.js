// Visual Studio references

/// <reference path="jquery-1.9.1.min.js" />
/// <reference path="jquery-ui-1.10.2.min.js" />
/// <reference path="moment.min.js" />
/// <reference path="timelineScheduler.js" />

var today = moment().startOf('day');
var itemCount = 0;
var prjCount = 0;

var Calendar = {
    Periods: [
        {
            Name: '3 days',
            Label: '3 days',
            TimeframePeriod: (1),
            TimeframeOverall: (3),
            TimeframeHeaders: [
                'Do MMM',
                'Do'
            ],
            Classes: 'period-3day'
        },
        {
            Name: '1 week',
            Label: '1 week',
            TimeframePeriod: (1),
            TimeframeOverall: (7),
            TimeframeHeaders: [
                'MMM',
                'Do'
            ],
            Classes: 'period-1week'
        },
        {
            Name: '1 month',
            Label: '1 month',
            TimeframePeriod: (1),
            TimeframeOverall: (31),
            TimeframeHeaders: [
                'MMM',
                'Do'
            ],
            Classes: 'period-1month'
        },
        {
            Name: '1 quarter',
            Label: '1 quarter',
            TimeframePeriod: (1),
            TimeframeOverall: (7*13),
            TimeframeHeaders: [
                'MMM',
                'W'
            ],
            Classes: 'period-1quarter'
        },
        {
            Name: '1 year',
            Label: '1 year',
            TimeframePeriod: (1),
            TimeframeOverall: (365),
            TimeframeHeaders: [
                'Q',
                'MMM'
            ],
            Classes: 'period-1year'
        },
        {
            Name: '5 year',
            Label: '5 year',
            TimeframePeriod: (1),
            TimeframeOverall: (365*5),
            TimeframeHeaders: [
                'YYYY',
                'MMM'
            ],
            Classes: 'period-5year'
        }
    ],

    Items: [
        {
            id: 0,
            name: '<div>Item 1</div><div>Sub Info</div>',
            sectionID: 0,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            top: 0,
            left: 0,
            classes: 'item-status-three',
            events: [
                {
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                },
                {
                    label: 'two',
                    at: moment(today).add('hours', 10),
                    classes: 'item-event-two'
                },
                {
                    label: 'three',
                    at: moment(today).add('hours', 11),
                    classes: 'item-event-three'
                }
            ]
        },
        {
            id: 1,
            name: '<div>Item 3</div>',
            start: moment(today).add('hours', 12),
            end: moment(today).add('days', 3).add('hours', 4),
            sectionID: 1,
            classes: 'item-status-none'
        },
        {
            id: 2,
            name: '<div>Item 2</div><div>Sub Info</div>',
            sectionID: 2,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-one',
            events: [
                {
                    icon: '',
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                }
            ]
        },
        {
            id: 3,
            name: '<div>Item 1</div><div>Sub Info</div>',
            sectionID: 3,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-three',
            events: [
                {
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                },
                {
                    label: 'two',
                    at: moment(today).add('hours', 10),
                    classes: 'item-event-two'
                },
                {
                    label: 'three',
                    at: moment(today).add('hours', 11),
                    classes: 'item-event-three'
                }
            ]
        },
        {
            id: 4,
            name: '<div>Item 3</div>',
            start: moment(today).add('hours', 12),
            end: moment(today).add('days', 3).add('hours', 4),
            sectionID: 4,
            classes: 'item-status-none'
        },
        {
            id: 5,
            name: '<div>Item 2</div><div>Sub Info</div>',
            sectionID: 5,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-one',
            events: [
                {
                    icon: '',
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                }
            ]
        },
        {
            id: 6,
            name: '<div>Item 3</div>',
            start: moment(today).add('hours', 12),
            end: moment(today).add('days', 3).add('hours', 4),
            sectionID: 6,
            classes: 'item-status-none'
        },
        {
            id: 7,
            name: '<div>Item 2</div><div>Sub Info</div>',
            sectionID: 7,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-one',
            events: [
                {
                    icon: '',
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                }
            ]
        },
        {
            id: 8,
            name: '<div>Item 1</div><div>Sub Info</div>',
            sectionID: 8,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-three',
            events: [
                {
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                },
                {
                    label: 'two',
                    at: moment(today).add('hours', 10),
                    classes: 'item-event-two'
                },
                {
                    label: 'three',
                    at: moment(today).add('hours', 11),
                    classes: 'item-event-three'
                }
            ]
        },
        {
            id: 9,
            name: '<div>Item 3</div>',
            start: moment(today).add('hours', 12),
            end: moment(today).add('days', 3).add('hours', 4),
            sectionID: 9,
            classes: 'item-status-none'
        },
        {
            id: 10,
            name: '<div>Item 2</div><div>Sub Info</div>',
            sectionID: 9,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-one',
            events: [
                {
                    icon: '',
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                }
            ]
        },{
            id: 11,
            name: '<div>Item 1</div><div>Sub Info</div>',
            sectionID: 10,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-three',
            events: [
                {
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                },
                {
                    label: 'two',
                    at: moment(today).add('hours', 10),
                    classes: 'item-event-two'
                },
                {
                    label: 'three',
                    at: moment(today).add('hours', 11),
                    classes: 'item-event-three'
                }
            ]
        },
        {
            id: 12,
            name: '<div>Item 3</div>',
            start: moment(today).add('hours', 12),
            end: moment(today).add('days', 3).add('hours', 4),
            sectionID: 11,
            classes: 'item-status-none'
        },
        {
            id: 13,
            name: '<div>Item 2</div><div>Sub Info</div>',
            sectionID: 11,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-one',
            events: [
                {
                    icon: '',
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                }
            ]
        },
        {
            id: 14,
            name: '<div>Item 1</div><div>Sub Info</div>',
            sectionID: 13,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-three',
            events: [
                {
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                },
                {
                    label: 'two',
                    at: moment(today).add('hours', 10),
                    classes: 'item-event-two'
                },
                {
                    label: 'three',
                    at: moment(today).add('hours', 11),
                    classes: 'item-event-three'
                }
            ]
        },
        {
            id: 15,
            name: '<div>Item 3</div>',
            start: moment(today).add('hours', 12),
            end: moment(today).add('days', 3).add('hours', 4),
            sectionID: 14,
            classes: 'item-status-none'
        },
        {
            id: 16,
            name: '<div>Item 2</div><div>Sub Info</div>',
            sectionID: 15,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-one',
            events: [
                {
                    icon: '',
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                }
            ]
        },
        {
            id: 17,
            name: '<div>Item 3</div>',
            start: moment(today).add('hours', 12),
            end: moment(today).add('days', 3).add('hours', 4),
            sectionID: 16,
            classes: 'item-status-none'
        },
        {
            id: 18,
            name: '<div>Item 2</div><div>Sub Info</div>',
            sectionID: 17,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-one',
            events: [
                {
                    icon: '',
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                }
            ]
        },
        {
            id: 19,
            name: '<div>Item 1</div><div>Sub Info</div>',
            sectionID: 19,
            start: moment(today).add('days', -1),
            end: moment(today).add('days', 3),
            classes: 'item-status-three',
            events: [
                {
                    label: 'one',
                    at: moment(today).add('hours', 6),
                    classes: 'item-event-one'
                },
                {
                    label: 'two',
                    at: moment(today).add('hours', 10),
                    classes: 'item-event-two'
                },
                {
                    label: 'three',
                    at: moment(today).add('hours', 11),
                    classes: 'item-event-three'
                }
            ]
        },
        {
            id: 20,
            name: '<div>Item 3</div>',
            start: moment(''),// moment(today).add('hours', 12),
            end: moment(today).add('days', 3).add('hours', 4),
            sectionID: 19,
            classes: 'item-status-none'
        }
    ],

    Sections: [
        {
            id: 0,
            name: 'Section 1'
        },
        {
            id: 1,
            name: 'Section 2'
        },
        {
            id: 2,
            name: 'Section 3'
        },
        {
            id: 3,
            name: 'Section 4'
        },
        {
            id: 4,
            name: 'Section 5'
        },
        {
            id: 5,
            name: 'Section 6'
        },
        {
            id: 6,
            name: 'Section 7'
        },
        {
            id: 7,
            name: 'Section 8'
        },
        {
            id: 8,
            name: 'Section 9'
        },
        {
            id: 9,
            name: 'Section 10'
        },        {
            id: 10,
            name: 'Section 1'
        },
        {
            id: 11,
            name: 'Section 2'
        },
        {
            id: 12,
            name: 'Section 3'
        },
        {
            id: 13,
            name: 'Section 4'
        },
        {
            id: 14,
            name: 'Section 5'
        },
        {
            id: 15,
            name: 'Section 6'
        },
        {
            id: 16,
            name: 'Section 7'
        },
        {
            id: 17,
            name: 'Section 8'
        },
        {
            id: 18,
            name: 'Section 9'
        },
        {
            id: 19,
            name: 'Section 10'
        }
    ],

    Init: function (start) {
        TimeScheduler.Options.GetSections = Calendar.GetSections;
        TimeScheduler.Options.GetSchedule = Calendar.GetSchedule;
        // TimeScheduler.Options.Start = today;
        // TimeScheduler.Options.Start = moment(today).add('day', -3);
        TimeScheduler.Options.Start = moment(start);
        TimeScheduler.Options.Periods = Calendar.Periods;
        TimeScheduler.Options.SelectedPeriod = '1 quarter';
        TimeScheduler.Options.Element = $('.calendar');

        TimeScheduler.Options.AllowDragging = true;
        TimeScheduler.Options.AllowResizing = true;

        TimeScheduler.Options.Events.ItemClicked = Calendar.Item_Clicked;
        TimeScheduler.Options.Events.ItemDropped = Calendar.Item_Dragged;
        TimeScheduler.Options.Events.ItemResized = Calendar.Item_Resized;

        TimeScheduler.Options.Events.ItemMovement = Calendar.Item_Movement;
        TimeScheduler.Options.Events.ItemMovementStart = Calendar.Item_MovementStart;
        TimeScheduler.Options.Events.ItemMovementEnd = Calendar.Item_MovementEnd;

        TimeScheduler.Options.Text.NextButton = '&nbsp;';
        TimeScheduler.Options.Text.PrevButton = '&nbsp;';

        TimeScheduler.Options.MaxHeight = 100;

        // console.log('calendar2.js Init1');
        TimeScheduler.Init();
    },

    GetTableRows: function () {
        var sectionCnt = TimeScheduler.GetInSections().length;
        return sectionCnt;
    },

    AppendNextPage: function (page) {
        TimeScheduler.AppendNextPage(page);
    },

    GetSections: function (callback) {
        console.log('GetSections');
        callback(Calendar.Sections);
    },

    GetSchedule: function (callback, start, end) {
        console.log('GetSchedule');
        callback(Calendar.Items);
    },

    Item_Clicked: function (item) {
        console.log(item);
    },

    Item_Dragged: function (item, sectionID, start, end) {
        var foundItem;

        // console.log(item);
        // console.log(sectionID);
        // console.log(start);
        // console.log(end);

        for (var i = 0; i < Calendar.Items.length; i++) {
            foundItem = Calendar.Items[i];

            if (foundItem.id === item.id) {
                foundItem.sectionID = sectionID;
                foundItem.start = start;
                foundItem.end = end;

                Calendar.Items[i] = foundItem;
            }
        }

        // console.log('calendar2.js Init2');
        TimeScheduler.Init();
    },

    Item_Resized: function (item, start, end) {
        var foundItem;

        // console.log(item);
        // console.log(start);
        // console.log(end);

        for (var i = 0; i < Calendar.Items.length; i++) {
            foundItem = Calendar.Items[i];

            if (foundItem.id === item.id) {
                foundItem.start = start;
                foundItem.end = end;

                Calendar.Items[i] = foundItem;
            }
        }

        // console.log('calendar2.js Init3');
        TimeScheduler.Init();
    },

    Item_Movement: function (item, start, end) {
        var html;

        html =  '<div>';
        html += '   <div>';
        html += '       Start: ' + start.format('Do MMM YYYY HH:mm');
        html += '   </div>';
        html += '   <div>';
        html += '       End: ' + end.format('Do MMM YYYY HH:mm');
        html += '   </div>';
        html += '</div>';

        $('.realtime-info').empty().append(html);
        console.log('Start: ' + start.format('Do MMM YYYY HH:mm'));
        console.log('End: ' + end.format('Do MMM YYYY HH:mm'));
    },

    Item_MovementStart: function () {
        $('.realtime-info').show();
    },

    Item_MovementEnd: function () {
        $('.realtime-info').hide();
    },

    setItems: function (val_items, val_sections, start, due) {
        Calendar.Items = [];
        Calendar.prjCount = 0;
        // console.log(val_sections);
        // console.log(val_items);

        var allItems = {};
        for (i=0; i<val_sections.length; i++) {
            
        }

        for (i=0; i<val_sections.length; i++) {
            var obj = {};
            obj.id = val_sections[i].id;
            obj.name = '<div>'+val_sections[i].name+'</div>';
            obj.sectionID = val_sections[i].id;
            obj.start = moment(val_sections[i].startDate);
            obj.end = moment(val_sections[i].dueDate);
            obj.top = 0;
            obj.left = 0;
            obj.classes = 'item-status-none';

            Calendar.Items.push(obj);
            // console.log(obj);
            Calendar.prjCount++;

            for (j=0; j<val_items.length; j++) {
                if (val_sections[i].id == val_items[j].prjid) {
                    // if (moment(val_items[j].startDate).isBetween(moment(start), moment(due)) 
                    //     || moment(val_items[j].dueDate).isBetween(moment(start), moment(due))
                    //     || moment(val_items[j].startDate).isSame(moment(start))
                    //     || moment(val_items[j].startDate).isSame(moment(due))
                    //     || moment(val_items[j].dueDate).isSame(moment(start))
                    //     || moment(val_items[j].dueDate).isSame(moment(due))
                    //     || moment(start).isBetween(moment(val_items[j].startDate), moment(val_items[j].dueDate)) 
                    //     || moment(due).isBetween(moment(val_items[j].startDate), moment(val_items[j].dueDate)) 
                    //     || moment(start).isSame(moment(val_items[j].startDate))
                    //     || moment(start).isSame(moment(val_items[j].dueDate))
                    //     || moment(due).isSame(moment(val_items[j].startDate))
                    //     || moment(due).isSame(moment(val_items[j].dueDate))
                    //     ) {
                        var obj = {};
                        obj.id = val_items[j].id;
                        obj.name = '<div>'+val_items[j].name+'</div>';
                        obj.sectionID = val_items[j].prjid;
                        obj.start = moment(val_items[j].startDate);
                        obj.end = moment(val_items[j].dueDate);
                        obj.top = 0;
                        obj.left = 0;
                        switch (val_items[j].status_id) {
                        case 0:
                            obj.classes = 'item-status-none';
                            break;
                        case 1:
                            obj.classes = 'item-status-one';
                            break;
                        case 2:
                            obj.classes = 'item-status-two';
                            break;
                        case 3:
                        default:
                            obj.classes = 'item-status-three';
                            break;
                        }
                        Calendar.Items.push(obj);
                    // }
                }
            }
        }
        
        // for (j=0; j< val_items.length; j++) {
        //     if (moment(val_items[j].startDate).isBetween(moment(start), moment(due)) 
        //         || moment(val_items[j].dueDate).isBetween(moment(start), moment(due))
        //         || moment(val_items[j].startDate).isSame(moment(start))
        //         || moment(val_items[j].startDate).isSame(moment(due))
        //         || moment(val_items[j].dueDate).isSame(moment(start))
        //         || moment(val_items[j].dueDate).isSame(moment(due))
        //         || moment(start).isBetween(moment(val_items[j].startDate), moment(val_items[j].dueDate)) 
        //         || moment(due).isBetween(moment(val_items[j].startDate), moment(val_items[j].dueDate)) 
        //         || moment(start).isSame(moment(val_items[j].startDate))
        //         || moment(start).isSame(moment(val_items[j].dueDate))
        //         || moment(due).isSame(moment(val_items[j].startDate))
        //         || moment(due).isSame(moment(val_items[j].dueDate))
        //         ) {
        //         if (startItem == 0) {
        //             var obj = {};
        //             obj.id = val_items[j].prjid;
        //             obj.name = '<div>'+val_items[j].prjname+'</div>';
        //             obj.sectionID = val_items[j].prjid;
        //             obj.start = moment(val_items[j].prjStart);
        //             obj.end = moment(val_items[j].prjDue);
        //             obj.classes = 'item-status-none';

        //             Calendar.Items.push(obj);
        //             // console.log(obj);
        //             Calendar.prjCount++;
        //             startItem++;
        //         }
        //         var obj = {};
        //         obj.id = val_items[j].id;
        //         obj.name = '<div>'+val_items[j].name+'</div>';
        //         obj.sectionID = val_items[j].prjid;
        //         obj.start = moment(val_items[j].startDate);
        //         obj.end = moment(val_items[j].dueDate);
        //         switch (val_items[j].status_id) {
        //         case 0:
        //             obj.classes = 'item-status-none';
        //             break;
        //         case 1:
        //             obj.classes = 'item-status-one';
        //             break;
        //         case 2:
        //             obj.classes = 'item-status-two';
        //             break;
        //         case 3:
        //         default:
        //             obj.classes = 'item-status-three';
        //             break;
        //         }
        //         Calendar.Items.push(obj);
        //         // console.log(obj);
        //     }
        // }
        // for (i=0; i<val_items.length; i++) {
        //     if (i==0 || val_items[i].prjid != val_items[i-1].prjid) {
        //         var obj = {};
        //         obj.id = val_items[i].prjid;
        //         obj.name = '<div>'+val_items[i].prjname+'</div>';
        //         obj.sectionID = val_items[i].prjid;
        //         obj.start = moment(val_items[i].prjStart);
        //         obj.end = moment(val_items[i].prjDue);
        //         obj.classes = 'item-status-none';

        //         Calendar.Items.push(obj);
        //         // console.log(obj);
        //         Calendar.prjCount++;
        //     }
        //     // console.log(i, 
        //     //     moment(val_items[i].startDate).isBetween(moment(start), moment(due)),
        //     //     moment(val_items[i].dueDate).isBetween(moment(start), moment(due)),
        //     //     moment(val_items[i].startDate).isSame(moment(start)),
        //     //     moment(val_items[i].startDate).isSame(moment(due)),
        //     //     moment(val_items[i].dueDate).isSame(moment(start)),
        //     //     moment(val_items[i].dueDate).isSame(moment(due)),
        //     //     moment(start).isBetween(moment(val_items[i].startDate), moment(val_items[i].dueDate)),
        //     //     moment(due).isBetween(moment(val_items[i].startDate), moment(val_items[i].dueDate)),
        //     //     moment(start).isSame(moment(val_items[i].startDate)),
        //     //     moment(start).isSame(moment(val_items[i].dueDate)),
        //     //     moment(due).isSame(moment(val_items[i].startDate)),
        //     //     moment(due).isSame(moment(val_items[i].dueDate)));
        //     if (moment(val_items[i].startDate).isBetween(moment(start), moment(due)) 
        //         || moment(val_items[i].dueDate).isBetween(moment(start), moment(due))
        //         || moment(val_items[i].startDate).isSame(moment(start))
        //         || moment(val_items[i].startDate).isSame(moment(due))
        //         || moment(val_items[i].dueDate).isSame(moment(start))
        //         || moment(val_items[i].dueDate).isSame(moment(due))
        //         || moment(start).isBetween(moment(val_items[i].startDate), moment(val_items[i].dueDate)) 
        //         || moment(due).isBetween(moment(val_items[i].startDate), moment(val_items[i].dueDate)) 
        //         || moment(start).isSame(moment(val_items[i].startDate))
        //         || moment(start).isSame(moment(val_items[i].dueDate))
        //         || moment(due).isSame(moment(val_items[i].startDate))
        //         || moment(due).isSame(moment(val_items[i].dueDate))
        //         ) {
        //         var obj = {};
        //         obj.id = val_items[i].id;
        //         obj.name = '<div>'+val_items[i].name+'</div>';
        //         obj.sectionID = val_items[i].prjid;
        //         obj.start = moment(val_items[i].startDate);
        //         obj.end = moment(val_items[i].dueDate);
        //         switch (val_items[i].status_id) {
        //         case 0:
        //             obj.classes = 'item-status-none';
        //             break;
        //         case 1:
        //             obj.classes = 'item-status-one';
        //             break;
        //         case 2:
        //             obj.classes = 'item-status-two';
        //             break;
        //         case 3:
        //         default:
        //             obj.classes = 'item-status-three';
        //             break;
        //         }
        //         Calendar.Items.push(obj);
        //         // console.log(obj);
        //     }
        //     // if (Calendar.Items.length >= 70)
        //     //     break;
        // }
        console.log('Calendar.Items:', Calendar.Items);
        
        // var test1 = Calendar.Items;
        // for (var idx=0; idx<val_sections.length; idx++) {
        //     var test2 = test1.findIndex(i => i.name == '<div>'+val_sections[idx].name+'</div>');
        //     console.log(test2, Calendar.Items[test2]);
        // }
    },

    setSections: function (val_sections) {
        Calendar.Sections = [];
        // var loopLen = 0;
        // if (Calendar.prjCount <= val_sections.length)
        //     loopLen = Calendar.prjCount;
        // else
        //     loopLen = val_sections.length;
        // console.log(Calendar.prjCount, val_sections.length, loopLen);

        // console.log(Calendar.prjCount, val_sections[Calendar.prjCount].name);
        for (i=0; i<val_sections.length; i++) {
        // for (i=0; i<Calendar.prjCount; i++) {
            // console.log(String(val_sections[i].id));
            // console.log(Calendar.Items.findIndex(i=>i.sectionID == String(val_sections[i].id)));
            var obj = {};
            obj.id = parseInt(val_sections[i].id);
            obj.name = val_sections[i].name;
            // if (obj.name == '삼성노트북_바이럴')
            //     console.log(i, obj.name);
            Calendar.Sections.push(obj);
        }
        // console.log(Calendar.Sections);
    }
};

// $(window).scroll(function () {
//     if ($(window).scrollTop() == $(document).height() - $(window).height()) {
//         console.log(++page);
//         $("enters").append("<h1>Page " + page + "</h1><br/>So")
//     }
// });
